<?php

class Accesos {

    static public function codigos(){

        $accesos = array(
            "inventario" => 1000,
            "clientes" => 1001,
            "servicioAlCliente" => 1002,
            "compras" => 1003,
            "materiales" => 1004,
            "cotizaciones" => 1005,
            "usuarios" => 1006,
            "ordenDeTrabajo" => 1007,
            "ventas" => 1008,
            "direcciones" => 1009
        );

        return $accesos;
    }

    static public function codAcciones(){

        $accesos = array(
            "AGREGAR" => 1,
            "ACTUALIZAR" => 2,
            "ELIMINAR" => 4,
            "VISUALIZAR" => 3
        );

        return $accesos;
    }

}