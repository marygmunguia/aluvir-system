<?php

require_once("../../../../../vendor/autoload.php");
require_once("../../../../../controllers/sistema/api.controlador.php");
require_once("../../../../../controllers/cotizaciones/cotizaciones.controlador.php");
require_once("../../../../../controllers/ordenDeTrabajo/ordenDeTrabajo.controlador.php");

require_once('../tcpdf_include.php');

class imprimirCotizacion
{

    public $codigo;

    public function traerInformacionCotizacion()
    {
        $cod = $this->codigo;
        $respuesta = ctrCotizacion::consultarCotizacionVenta($cod);

        $idOrdenDeTrabajo = $respuesta["PK_COD_ORDEN_TRABAJO"];
        $detalleOrden = $respuesta["DET_ORDEN_TRABAJO"];
        $cliente = $respuesta["NOM_CLIENTE"] . " " . $respuesta["APE_CLIENTE"];
        $fecha = date_format(date_create($respuesta["FEC_COTIZACION"]), "d-m-Y");

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'letter', true, 'UTF-8', false);

$pdf->AddPage();

$header = <<<EOD
<table>
	<tr>
		<td style="text-align:center; font-size: 25px; font-weight: bold;">ALUVIR SYSTEM</td>
	</tr>
	<tr>
        <td style="text-align:center;">
        Correo Electrónico: info@aluvirsystem.com<br>
        Contactos: +504 9865-8562 / +504 8652-8587
        </td>
    </tr>
	<tr>
		<td style="text-align:center;"><br><h2>ORDEN DE TRABAJO # $idOrdenDeTrabajo</h2><br></td>
	</tr>
</table>
EOD;

$pdf->writeHTML($header, false, false, false, false, '');

$tabla = <<<EOD
<table style="border-spacing: 0px 5px;">
	<tr>
        <td style="width: 50%;"><strong>Nombre del cliente:</strong> $cliente</td>
        <td style="width: 50%;"><strong>Fecha de Entrega:</strong> $fecha</td>
    </tr>
    <tr>
    <td style="width: 70%;"><strong>Detalles:</strong> $detalleOrden</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($tabla, false, false, false, false, '');


$cabezera = <<<EOD
<br><br>
<table style="padding: 5px ; border: 1px solid #000; background-color: #ccc; text-align: center;">
	<tr>
        <td style="border: 1px solid #000; width: 5%;">#</td>
        <td style="border: 1px solid #000; width: 25%;">Nombre</td>
        <td style="border: 1px solid #000; width: 55%;">Detalle</td>
        <td style="border: 1px solid #000; width: 15%;">Cantidad</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($cabezera, false, false, false, false, '');

$detalles = ctrOrdenDeTrabajo::consultarDetalleCotizacionImprimir($cod);

foreach ($detalles as $key => $value) {

$detalle = <<<EOD
<table style="padding-right: 5px ; border: 1px solid #000; font-size: 10px;">
	<tr>
        <td style="border: 1px solid #000; width: 5%;  height: 60px; text-align: center;">$value[COD_PRODUCTO]</td>
        <td style="border: 1px solid #000; width: 25%; height: 60px; ">$value[NOMBRE]</td>
        <td style="border: 1px solid #000; width: 55%; height: 60px; ">$value[DETALLE]</td>
        <td style="border: 1px solid #000; width: 15%; height: 60px;  text-align: center;">$value[CANT_PRODUCTO]</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($detalle, false, false, false, false, '');
}


$pdf->Output('ordenDeTrabajo.pdf', 'I');

    }
}

$cotizacion = new imprimirCotizacion;
$cotizacion->codigo = $_GET["codigo"];
$cotizacion->traerInformacionCotizacion();
