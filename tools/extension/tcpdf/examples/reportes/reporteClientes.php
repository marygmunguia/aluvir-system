<?php

require_once("../../../../../vendor/autoload.php");
require_once("../../../../../controllers/sistema/api.controlador.php");
require_once("../../../../../controllers/clientes/clientes.controlador.php");

require_once('../tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->AddPage();

$header = <<<EOD
<table>
	<tr>
		<td style="text-align:center;"><h2 style="font-size: 25px;">ALUVIR SYSTEM</h2></td>
	</tr>
	<tr>
        <td style="height: 5px;"></td>
    </tr>
	<tr>
		<td style="text-align:center;"><h3>REPORTE DE CLIENTES</h3></td>
	</tr>
</table>
EOD;

$pdf->writeHTML($header, false, false, false, false, '');

$headerTabla = <<<EOF
    <br><br>
	<table style="font-size:10px; padding:5px 10px; background-color: #03a3cc; color:#fff;">
		<tr>
        <th style="width:60px; text-align:center; border: 1px solid #000;"><strong>#</strong></th>
        <th style="width:230px; text-align:center; border: 1px solid #000;"><strong>Nombre Completo</strong></th>
        <th style="width:170px; text-align:center; border: 1px solid #000;"><strong>Correo Electrónico</strong></th>
		<th style="width:80px; text-align:center; border: 1px solid #000;"><strong>Celular</strong></th>
		</tr>
	</table>
EOF;

$pdf->writeHTML($headerTabla, false, false, false, false, '');

$respuesta = ctrClientes::consultarClientesReporte();

foreach ($respuesta as $key => $value) {

$body = <<<EOD
<table style="font-size:10px; padding:5px 10px;">
<tr>
<td style="width:60px; text-align:center; border: 1px solid #000;">$value[PK_COD_CLIENTE]</td>
<td style="width:230px; text-align:left; border: 1px solid #000;"><font style="text-transform: uppercase;">$value[NOM_CLIENTE] $value[APE_CLIENTE]</font></td>
<td style="width:170px; text-align:left; border: 1px solid #000;">$value[EMAIL]</td>
<td style="width:80px; text-align:left; border: 1px solid #000;">$value[CELULAR]</td>
</tr>
</table>
EOD;

$pdf->writeHTML($body, false, false, false, false, '');

}

$pdf->Output('reporteClientes.pdf', 'I');
