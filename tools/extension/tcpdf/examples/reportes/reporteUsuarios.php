<?php

require_once("../../../../../vendor/autoload.php");
require_once("../../../../../controllers/sistema/api.controlador.php");
require_once("../../../../../controllers/usuarios/usuarios.controlador.php");

require_once('../tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->AddPage();

$header = <<<EOD
<table>
	<tr>
		<td style="text-align:center;"><h2 style="font-size: 25px;">ALUVIR SYSTEM</h2></td>
	</tr>
	<tr>
        <td style="height: 5px;"></td>
    </tr>
	<tr>
		<td style="text-align:center;"><h3>REPORTE DE USUARIOS</h3></td>
	</tr>
</table>
EOD;

$pdf->writeHTML($header, false, false, false, false, '');

$headerTabla = <<<EOF
    <br><br>
	<table style="font-size:10px; padding:5px 10px; background-color: #03a3cc; color:#fff;">
		<tr>
        <th style="width:60px; text-align:center; border: 1px solid #000;"><strong>#</strong></th>
        <th style="width:200px; text-align:center; border: 1px solid #000;"><strong>Nombre</strong></th>
        <th style="width:120px; text-align:center; border: 1px solid #000;"><strong>Estado</strong></th>
        <th style="width:70px; text-align:center; border: 1px solid #000;"></th>
        <th style="width:90px; text-align:center; border: 1px solid #000;"># empleado</th>
		</tr>
	</table>
EOF;

$pdf->writeHTML($headerTabla, false, false, false, false, '');

$respuesta = ctrUsuarios::consultarUsuariosReporte();

foreach ($respuesta as $key => $value) {

$body = <<<EOD
<table style="font-size:10px; padding:5px 10px;">
<tr>
<td style="width:60px; text-align:center; border: 1px solid #000;">$value[PK_ID_USUARIO]</td>
<td style="width: 200px; text-align:left; border: 1px solid #000;"><font>$value[NOM_USUARIO]</font></td>
<td style="width:120px; text-align:left; border: 1px solid #000;">$value[EST_USUARIO]</td>
<td style="width:70px; text-align:left; border: 1px solid #000;"><img style="width: 50px; height: auto;" src="../../../../../$value[IMG_USUARIO]"/></td>
<td style="width:90px; text-align:center; border: 1px solid #000;">$value[ID_EMPLEADO]</td>
</tr>
</table>
EOD;

$pdf->writeHTML($body, false, false, false, false, '');

}

$pdf->Output('reporteClientes.pdf', 'I');
