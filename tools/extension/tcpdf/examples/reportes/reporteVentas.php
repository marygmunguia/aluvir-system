<?php

require_once("../../../../../vendor/autoload.php");
require_once("../../../../../controllers/sistema/api.controlador.php");
require_once("../../../../../controllers/ventas/ventas.controlador.php");

require_once('../tcpdf_include.php');

class generarReportePorFecha
{

	public $inicio;
	public $final;

	public function reporteDeVentaPorFecha()
	{

		$finicio = $this->inicio;
		$ffinal = $this->final;

		$FechaInicio = date_format(date_create($finicio), "d-m-Y");
		$FechaFinal = date_format(date_create($ffinal), "d-m-Y");


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->AddPage();

$header = <<<EOD
<table>
	<tr>
		<td style="text-align:center;"><h2 style="font-size: 25px;">ALUVIR SYSTEM</h2></td>
	</tr>
	<tr>
        <td style="height: 4px;"></td>
    </tr>
	<tr>
		<td style="text-align:center;"><h3>REPORTE DE VENTAS</h3></td><br>
	</tr>
	<tr>
	    <td style="text-align:center;"><h4>DEL $FechaInicio AL $FechaFinal</h4></td>
	</tr>
</table>
EOD;

$pdf->writeHTML($header, false, false, false, false, '');

$headerTabla = <<<EOF
    <br><br>
	<table style="font-size:10px; padding:5px 10px; background-color: #03a3cc; color:#fff;">
		<tr>
        <th style="width:60px; text-align:center; border: 1px solid #000;"><strong>#</strong></th>
        <th style="width:150px; text-align:center; border: 1px solid #000;"><strong>Cliente</strong></th>
        <th style="width:80px; text-align:center; border: 1px solid #000;"><strong>Tipo Pago</strong></th>
		<th style="width:100px; text-align:center; border: 1px solid #000;"><strong>Metodo Pago</strong></th>
		<th style="width:70px; text-align:center; border: 1px solid #000;"><strong>Subtotal</strong></th>
		<th style="width:70px; text-align:center; border: 1px solid #000;"><strong>Total</strong></th>
		</tr>
	</table>
EOF;

$pdf->writeHTML($headerTabla, false, false, false, false, '');

$respuesta = ctrVentas::reportePorFechas($finicio, $ffinal);

foreach ($respuesta as $key => $value) {

	    $cliente = $value["NOM_CLIENTE"] . " " . $value["APE_CLIENTE"];
        $subtotal = number_format($value["SUBTOTAL"], 2);
        $total_pagar = number_format($value["TOTAL_PAGAR"], 2);
        $tipoPago = ucwords(strtolower($value["TIPO_PAGO"]));
        $metodoPago = $value["NOM_METODO_PAGO"];

$body = <<<EOD
	<table style="font-size:10px; padding:5px 10px;">
	<tr>
	<th style="width:60px; text-align:center; border: 1px solid #000;">$value[PK_COD_VENTA]</th>
    <th style="width:150px; border: 1px solid #000;">$cliente</th>
    <th style="width:80px; border: 1px solid #000;">$tipoPago</th>
	<th style="width:100px; border: 1px solid #000;">$metodoPago</th>
	<th style="width:70px; text-align:center; border: 1px solid #000;">$subtotal</th>
	<th style="width:70px; text-align:center; border: 1px solid #000;">$total_pagar</th>
	</tr>
	</table>
EOD;
	
$pdf->writeHTML($body, false, false, false, false, '');
	
	}

$pdf->Output('reporteVentas.pdf', 'I');

	}
}

$reporte = new generarReportePorFecha;
$reporte->inicio = $_GET["inicio"];
$reporte->final = $_GET["final"];
$reporte->reporteDeVentaPorFecha();
