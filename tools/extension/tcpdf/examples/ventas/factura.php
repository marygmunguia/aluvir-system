<?php

require_once("../../../../../vendor/autoload.php");
require_once("../../../../../controllers/sistema/api.controlador.php");
require_once("../../../../../controllers/ventas/ventas.controlador.php");
require_once("../../../../../controllers/ordenDeTrabajo/ordenDeTrabajo.controlador.php");

require_once('../tcpdf_include.php');

class imprimirCotizacion
{

    public $codigo;

    public function traerInformacionCotizacion()
    {
        $cod = $this->codigo;
        $respuesta = ctrVentas::consultarVenta($cod);

        $idCotizacion = $respuesta["PK_COD_COTIZACION"];
        $idventa = $respuesta["PK_COD_VENTA"];
        $empleado = ucwords(strtolower($respuesta["NOMBRE"])) . " " . ucwords(strtolower($respuesta["APELLIDO"]));
        $cliente = $respuesta["NOM_CLIENTE"] . " " . $respuesta["APE_CLIENTE"];
        $fecha = date_format(date_create($respuesta["FEC_VENTA"]), "d-m-Y");
        $hora = date_format(date_create($respuesta["HORA_VENTA"]), "h:i a");
        $subtotal = number_format($respuesta["SUBTOTAL"], 2);
        $descuento = number_format($respuesta["DESCUENTO"], 2);
        $isv = number_format($respuesta["ISV"], 2);
        $total_pagar = number_format($respuesta["TOTAL_PAGAR"], 2);
        $tipoPago = ucwords(strtolower($respuesta["TIPO_PAGO"]));
        $metodoPago = ucwords(strtolower($respuesta["NOM_METODO_PAGO"]));

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'letter', true, 'UTF-8', false);

$pdf->AddPage();

$header = <<<EOD
<table>
	<tr>
		<td style="text-align:center; font-size: 25px; font-weight: bold;">ALUVIR SYSTEM</td>
	</tr>
	<tr>
        <td style="text-align:center;">
        RTN: 08012010258741<br>
        Correo Electrónico: info@aluvirsystem.com<br>
        Contactos: +504 9865-8562 / +504 8652-8587
        </td>
    </tr>
	<tr>
		<td style="text-align:center;"><br><h2>FACTURA DE VENTA # $idventa</h2><br></td>
	</tr>
</table>
EOD;

$pdf->writeHTML($header, false, false, false, false, '');

$tabla = <<<EOD
<table style="border-spacing: 0px 5px;">
	<tr>
        <td style="width: 50%;"><strong>Nombre del empleado:</strong> $empleado</td>
        <td style="width: 30%;"><strong>Fecha:</strong> $fecha</td>
        <td style="width: 20%;"><strong>Hora:</strong> $hora</td>
    </tr>
    <tr>
        <td style="width: 50%;"><strong>Tipo de pago:</strong> $tipoPago</td>
        <td style="width: 50%;"><strong>Método de pago:</strong> $metodoPago</td>
    </tr>
    <tr>
        <td style="width: 100%;"><strong>Nombre del cliente:</strong> $cliente</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($tabla, false, false, false, false, '');


$cabezera = <<<EOD
<br><br>
<table style="padding: 5px ; border: 1px solid #000; background-color: #ccc; text-align: center;">
	<tr>
        <td style="border: 1px solid #000; width: 5%;">#</td>
        <td style="border: 1px solid #000; width: 20%;">Nombre</td>
        <td style="border: 1px solid #000; width: 30%;">Detalle</td>
        <td style="border: 1px solid #000; width: 15%;">Cantidad</td>
        <td style="border: 1px solid #000; width: 15%;">Precio</td>
        <td style="border: 1px solid #000; width: 15%;">Total</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($cabezera, false, false, false, false, '');

$detalles = ctrOrdenDeTrabajo::consultarDetalleCotizacionImprimir($idCotizacion);

foreach ($detalles as $key => $value) {
    $precio = number_format($value["PRECIO_PRODUCTO"], 2);
    $total = number_format($value["TOTAL_PAGAR_PRODUCTO"], 2);

$detalle = <<<EOD
<table style="padding-right: 5px ; border: 1px solid #000; font-size: 10px;">
	<tr>
        <td style="border: 1px solid #000; width: 5%;  height: 60px; text-align: center;">$value[COD_PRODUCTO]</td>
        <td style="border: 1px solid #000; width: 20%; height: 60px; ">$value[NOMBRE]</td>
        <td style="border: 1px solid #000; width: 30%; height: 60px; ">$value[DETALLE]</td>
        <td style="border: 1px solid #000; width: 15%; height: 60px;  text-align: center;">$value[CANT_PRODUCTO]</td>
        <td style="border: 1px solid #000; width: 15%; height: 60px;  text-align: center;">$precio lps.</td>
        <td style="border: 1px solid #000; width: 15%; height: 60px;  text-align: center;">$total lps.</td>
    </tr>
</table>
EOD;

$pdf->writeHTML($detalle, false, false, false, false, '');

}

$total = <<<EOD
<br>
<br>
<h4 style="text-align: right;">DETALLES DE PAGO</h4>
<table style="padding: 5px;">
	<tr>
        <td style="width: 30%;"></td>
        <td style="background-color: #ccc; border: 1px solid #000; width: 40%;"><strong>Subtotal: </strong></td>
        <td style="border: 1px solid #000; width: 30%; text-align: center;">$subtotal</td>
    </tr>
    <tr>
        <td style="width: 30%;"></td>
        <td style="background-color: #ccc; border: 1px solid #000; width: 40%;"><strong>Descuentos: </strong></td>
        <td style="border: 1px solid #000; width: 30%; text-align: center;">- $descuento</td>
    </tr>
    <tr>
        <td style="width: 30%;"></td>
        <td style="background-color: #ccc; border: 1px solid #000; width: 40%;"><strong>Impuesto sobre venta (ISV): </strong></td>
        <td style="border: 1px solid #000; width: 30%; text-align: center;">$isv</td>
    </tr>
    <tr>
        <td style="width: 30%;"></td>
        <td style="background-color: #ccc; border: 1px solid #000; width: 40%;"><strong>Total a pagar: </strong></td>
        <td style="border: 1px solid #000; width: 30%; text-align: center;"><strong>$total_pagar</strong></td>
    </tr>
</table>
EOD;

$pdf->writeHTML($total, false, false, false, false, '');


$pdf->Output('facturaVenta.pdf', 'I');

}
}

$cotizacion = new imprimirCotizacion;
$cotizacion->codigo = $_GET["codigo"];
$cotizacion->traerInformacionCotizacion();
