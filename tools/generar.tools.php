<?php

class ctrGenerar
{

    public static function generarDatos()
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(20 / strlen($x)))), 1, 20);
    }

    public static function encriptar($dato){
        $encriptar = crypt($dato, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
        return $encriptar;
    }
}
