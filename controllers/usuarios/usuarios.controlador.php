<?php

use GuzzleHttp\Client;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


class ctrUsuarios
{
    // Consultar todos los usuarios
    public static function consultarUsuarios()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'usuarios/obtenerUsuarios');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    public static function consultarUsuariosReporte()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'usuarios/obtenerUsuariosReporte');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }

    // Consultar los accesos de sistema
    public static function consultarAccesos()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'sistema/consultarAcceso');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    //
    public function guardarUsuario()
    {
        if (isset($_POST["nombre"])) {

            $nombre = $_POST["nombre"];
            $apellido = $_POST["apellido"];
            $fec_nacimiento = $_POST["fec_nacimiento"];
            $sexo = $_POST["sexo"];
            $direccion = $_POST["direccion"];
            $telefono = $_POST["telefono"];
            $celular = $_POST["celular"];
            $correo = $_POST["correo"];
            $accesos = $_POST["accesos"];


            if (count($accesos) > 0) {

                $base_uri = ApiConnection::Api();

                try {

                    $cliente = new Client([
                        'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                    ]);

                    $cliente->post(
                        $base_uri . 'empleados/agregarEmpleado',
                        [
                            'body' => json_encode([
                                "nombres" => $nombre,
                                "apellidos" => $apellido,
                                "fechaNac" => $fec_nacimiento,
                                "sexo" => $sexo,
                                "direccion" => $direccion,
                                "telefono" => $telefono,
                                "celular" => $celular,
                                "email" => $correo
                            ])
                        ]
                    );

                    // CONSULTAR CODIGO DE EMPLEADO
                    try {

                        $cliente = new Client([
                            'headers' => ['Authorization' => $_SESSION["TOKEN"]]
                        ]);

                        $respuesta = $cliente->get($base_uri . 'empleados/consultarUltimoEmpleado');

                        $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

                        $idempleado =  $respuesta_api["respuesta"][0][0]["PK_COD_EMPLEADO"];

                        // AGREGAR USUARIO
                        try {

                            $usuario = strtolower($nombre) . strtolower($apellido) . $idempleado;
                            $pass = ctrGenerar::generarDatos();

                            $cliente = new Client([
                                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                            ]);

                            $cliente->post(
                                $base_uri . 'usuarios/agregarUsuario',
                                [
                                    'body' => json_encode([
                                        "nombre" => $usuario,
                                        "pass" => $pass,
                                        "img" => "views/img/default-150x150.png",
                                        "estado" => "ACTIVO",
                                        "codEmpleado" => $idempleado,
                                        "Admin" => $_SESSION["ID_USUARIO"]
                                    ])
                                ]
                            );

                            // CONSULTAR CODIGO DE USUARIO
                            try {

                                $cliente = new Client([
                                    'headers' => ['Authorization' => $_SESSION["TOKEN"]]
                                ]);

                                $respuesta = $cliente->get($base_uri . 'usuarios/consultarUltimoUsuario');

                                $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

                                $idusuario =  $respuesta_api["respuesta"][0][0]["PK_ID_USUARIO"];

                                // AGREGAR PERMISOS DE ACCESOS AL SISTEMA
                                try {

                                    for ($i = 0; $i < count($accesos); $i++) {

                                        $cliente = new Client([
                                            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                                        ]);

                                        $cliente->post(
                                            $base_uri . 'sistema/otorgarPermiso',
                                            [
                                                'body' => json_encode([
                                                    "usuario" => $idusuario,
                                                    "acceso" => $accesos[$i]
                                                ])
                                            ]
                                        );
                                    }

                                    // ENVIO DE CREDENCIALES

                                    $mail = new PHPMailer;

                                    $mail->Charset = "UTF-8";

                                    $mail->isMail();

                                    $mail->setFrom("info@aluvirsystem.com", "ALUVIR SYSTEM");

                                    $mail->addReplyTo("info@aluvirsystem.com", "ALUVIR SYSTEM");

                                    $mail->Subject  = "BIENVENIDO AL SISTEMA";

                                    $mail->addAddress($correo);

                                    $mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
        
                                <div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
                                
                                <center>
                                    <h3 style="font-weight:100; color:#999">NUEVO USUARIO REGISTRADO</h3>
    
                                    <hr style="border:1px solid #ccc; width:80%">
    
                                    <h4 style="font-weight:100; color:#999; padding:0 20px">TU USUARIO DE SISTEMA ES: ' . $usuario . '</h4>
                                    <h4 style="font-weight:100; color:#999; padding:0 20px">TU CLAVE DE ACCESO ES: ' . $pass . '</h4>
    
                                    <br>
    
                                    <hr style="border:1px solid #ccc; width:80%">
    
                                    <h5 style="font-weight:100; color:#999"></h5>
    
                                </center>	
    
                                </div>
    
                                </div>');

                                    $mail->Send();

?>
                                    <script LANGUAGE="javascript">
                                        $(document).ready(function() {
                                            Swal.fire({
                                                title: 'Guardado!',
                                                text: "Se ha agregado correctamente el nuevo registro",
                                                icon: 'success',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Ok'
                                            }).then((result) => {
                                                if (result.isConfirmed) {
                                                    <?php
                                                    $ruta = ctrRuta::cargarRuta();
                                                    ?>
                                                    window.location = "<?php echo $ruta; ?>usuarios";
                                                }
                                            })
                                        });
                                    </script>
                <?php

                                } catch (GuzzleHttp\Exception\ClientException $e) {
                                    print_r($e);
                                }
                            } catch (GuzzleHttp\Exception\ClientException $e) {
                                print_r($e);
                            }
                        } catch (GuzzleHttp\Exception\ClientException $e) {
                            print_r($e);
                        }
                    } catch (GuzzleHttp\Exception\ClientException $e) {
                        print_r($e);
                    }
                } catch (GuzzleHttp\Exception\ClientException $e) {
                    print_r($e);
                }
            } else {

                ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "Debe de elegir al menos un módulo de acceso para el usuario",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>usuarios";
                            }
                        })
                    });
                </script>
            <?php

            }
        }
    }


    // Consultar todos los empleados
    public static function consultarEmpleados()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'empleados/consultarEmpleados');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    public static function CambiarPassword()
    {
        if (isset($_POST["claveActual"])) {

            $claveActual = $_POST["claveActual"];
            $nuevaClave = $_POST["password"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->post(
                    $base_uri . 'usuarios/cambiarClave',
                    [
                        'body' => json_encode([
                            "codigo" => $_SESSION["ID_USUARIO"],
                            "nombre" => $_SESSION["NOMBRE_USUARIO"],
                            "clave" => $claveActual,
                            "claveNueva" => $nuevaClave,
                        ])
                    ]
                );

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Éxito!',
                            text: "Haz actualizado tu contraseña correctamente",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>perfil";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "No se ha actualizado correctamente contraseña. Asegurate que haz ingresado la contraseña correcta.",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>perfil";
                            }
                        })
                    });
                </script>
            <?php

            }
        }
    }


    public static function RecuperarPassword()
    {
        if (isset($_POST["email"])) {

            $email = $_POST["email"];
            $pass = ctrGenerar::generarDatos();

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'Authorization' => $_SESSION["TOKEN"]
                ]);

                $cliente->get(
                    $base_uri . 'usuarios/recuperarAcceso',
                    [
                        'query' => [
                            "CORREO" => $email,
                            "CLAVE" => $pass
                        ]
                    ]
                );

                // ENVIO DE CREDENCIALES

                $mail = new PHPMailer;

                $mail->Charset = "UTF-8";

                $mail->isMail();

                $mail->setFrom("info@aluvirsystem.com", "ALUVIR SYSTEM");

                $mail->addReplyTo("info@aluvirsystem.com", "ALUVIR SYSTEM");

                $mail->Subject  = "RECUPERAR ACCESO AL SISTEMA";

                $mail->addAddress($email);

                $mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">

            <div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
            
            <center>
                <h3 style="font-weight:100; color:#999">CAMBIO DE CLAVE DE ACCESO</h3>

                <hr style="border:1px solid #ccc; width:80%">

                <h4 style="font-weight:100; color:#999; padding:0 20px">TU CLAVE DE ACCESO TEMPORAL ES: ' . $pass . '</h4>

                <br>

                <hr style="border:1px solid #ccc; width:80%">

                <h5 style="font-weight:100; color:#999"></h5>

            </center>	

            </div>

            </div>');

                $mail->Send();

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Éxito!',
                            text: "Haz recuperado tu acceso correctamente, revisa tu correo electronico",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>login";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "No se ha recuperado correctamente contraseña. Asegurate que haz ingresado un correo válido.",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>login";
                            }
                        })
                    });
                </script>
            <?php

            }
            
        }
    }
}
