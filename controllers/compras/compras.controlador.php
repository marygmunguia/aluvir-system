<?php

use GuzzleHttp\Client;

class ctrCompras
{

    public static function consultarProveedores()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'proveedores/consultarProveedores');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    public static function consultarProveedor($cod)
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->post(
                $base_uri . 'proveedores/consultarProveedor',
                [
                    'body' => json_encode([
                        "cod" => $cod
                    ])
                ]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0]["NOM_PROVEEDOR"];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    public static function consultarCompras()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'compras/consultarCompras');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    public static function agregarCompra()
    {
        if (isset($_POST["codProveedor"])) {

            $base_uri = ApiConnection::Api();

            $factura = $_POST["numeroFactura"];
            $codProveedor = $_POST["codProveedor"];
            $fecha = $_POST["fechaCompra"];
            $subtotal = $_POST["subtotal"];
            if (isset($_POST["descuento"])) {
                $descuento = $_POST["descuento"];
            } else {
                $descuento = 0.00;
            }
            $isv = $_POST["isv"];
            $total = $_POST["total"];

            try {

                $cliente = new Client([
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

                $cliente->post(
                    $base_uri . 'compras/agregarCompra',
                    [
                        'body' => json_encode([
                            "FACTURA" => $factura,
                            "CODPROVEEDOR" => $codProveedor,
                            "FECHA" => $fecha,
                            "SUBTOTAL" => $subtotal,
                            "DESCUENTO" => $descuento,
                            "ISV" => $isv,
                            "TOTAL" => $total
                        ])
                    ]
                );

?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha agregado correctamente el nuevo registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>compras";
                            }
                        })
                    });
                </script>
            <?php


            } catch (GuzzleHttp\Exception\ClientException $e) {
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha agregado correctamente el nuevo registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>compras";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }

    public static function eliminarCompra()
    {
        if (isset($_POST["codigoCompra"])) {

            $codigo = $_POST["codigoCompra"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->delete(
                    $base_uri . 'compras/eliminarCompra',
                    [
                        'body' => json_encode([
                            "COD" => $codigo
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Borrado Exitoso!',
                            text: "Se ha eliminado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>compras";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "No se ha eliminado correctamente el registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>compras";
                            }
                        })
                    });
                </script>
            <?php

            }
        }
    }



    public static function actualizarCompra()
    {
        if (isset($_POST["codigoCompraE"])) {

            $base_uri = ApiConnection::Api();

            $codigo = $_POST["codigoCompraE"];
            $factura = $_POST["numeroFacturaE"];
            $codProveedor = $_POST["codProveedorE"];
            $fecha = $_POST["fechaCompraE"];
            $subtotal = $_POST["subtotalE"];
            if (isset($_POST["descuentoE"])) {
                $descuento = $_POST["descuentoE"];
            } else {
                $descuento = 0.00;
            }
            $isv = $_POST["isvE"];
            $total = $_POST["totalE"];

            try {

                $cliente = new Client([
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

                $cliente->put(
                    $base_uri . 'compras/actualizarCompra',
                    [
                        'body' => json_encode([
                            "COD" => $codigo,
                            "FACTURA" => $factura,
                            "CODPROVEEDOR" => $codProveedor,
                            "FECHA" => $fecha,
                            "SUBTOTAL" => $subtotal,
                            "DESCUENTO" => $descuento,
                            "ISV" => $isv,
                            "TOTAL" => $total
                        ])
                    ]
                );

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha actualizado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>compras";
                            }
                        })
                    });
                </script>
            <?php
            } catch (GuzzleHttp\Exception\ClientException $e) {
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha actualizado correctamente el registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>compras";
                            }
                        })
                    });
                </script>
<?php
            }
        }
    }
}
