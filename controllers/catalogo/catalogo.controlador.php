<?php

use GuzzleHttp\Client;

class ctrCatalogo
{
    //  CALCULAR DESCUENTO DE COTIZACION
    public static function calcularDescuento()
    {
        if (isset($_POST["porcentaje_descuento"]) && $_POST["porcentaje_descuento"] >= 0) {

            $_SESSION["porcentaje_descuento"] = 0;
            $_SESSION["porcentaje_descuento"] = $_POST["porcentaje_descuento"];
            $_SESSION["descuento"] = $_SESSION["sumaSubtotal"] * ($_SESSION["porcentaje_descuento"] / 100);

            $_SESSION["sumaSubtotal"] = 0;
            $_SESSION["isv"] = 0;
            $_SESSION["sumaTotal"] = 0;

            foreach ($_SESSION["cotizacionProductos"] as $key => $value) {
                $_SESSION["sumaSubtotal"] = $_SESSION["sumaSubtotal"] + ($value["PRECIO"] * $value[0]);
                $_SESSION["isv"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) * 0.15;
                $_SESSION["sumaTotal"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) + $_SESSION["isv"];
            }

?>
            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>realizarCotizacion";
                });
            </script>
            <?php

        } else {
            $_SESSION["descuento"] = 0;
        }
    }

    // AGREGAR PRODUCTO PERSONALIZADO A COTIZACION
    public static function nuevoProducto()
    {

        if (isset($_POST["nombre"])) {

            $nombre = $_POST["nombre"];
            $detalle = $_POST["detalle"];
            $precio = $_POST["precio"];
            $cantidad = $_POST["cantidad"];
            $imagen = 'views/img/default-150x150.png';

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->post(
                    $base_uri . 'catalogo/guardarProducto',
                    [
                        'body' => json_encode([
                            "nombre" => $nombre,
                            "detalle" => $detalle,
                            "imagen" => $imagen,
                            "precio" => $precio
                        ])
                    ]
                );

                try {

                    $cliente = new Client([
                        'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                    ]);

                    $respuesta = $cliente->get($base_uri . 'catalogo/consultarProductoAgregado');

                    $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

                    array_push($respuesta_api["respuesta"][0][0], $cantidad);

                    $_SESSION["cotizacionProductos"][count($_SESSION["cotizacionProductos"])] = $respuesta_api["respuesta"][0][0];

                    if (!isset($_SESSION["porcentaje_descuento"])){
                        $_SESSION["porcentaje_descuento"] = 0;
                    }
                    $_SESSION["sumaSubtotal"] = 0;
                    $_SESSION["isv"] = 0;
                    $_SESSION["sumaTotal"] = 0;

                    foreach ($_SESSION["cotizacionProductos"] as $key => $value) {
                        $_SESSION["sumaSubtotal"] = $_SESSION["sumaSubtotal"] + ($value["PRECIO"] * $value[0]);
                        $_SESSION["descuento"] = $_SESSION["sumaSubtotal"] * ($_SESSION["porcentaje_descuento"] / 100);
                        $_SESSION["isv"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) * 0.15;
                        $_SESSION["sumaTotal"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) + $_SESSION["isv"];
                    }
            ?>
                    <script LANGUAGE="javascript">
                        $(document).ready(function() {
                            Swal.fire({
                                title: 'Guardado!',
                                text: "Se ha agregado correctamente el nuevo registro",
                                icon: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    <?php
                                    $ruta = ctrRuta::cargarRuta();
                                    ?>
                                    window.location = "<?php echo $ruta; ?>realizarCotizacion";
                                }
                            })
                        });
                    </script>
                <?php
                } catch (GuzzleHttp\Exception\ClientException $e) {
                ?>
                    <script LANGUAGE="javascript">
                        $(document).ready(function() {
                            Swal.fire({
                                title: 'Error!',
                                text: "El producto se ha registrado pero NO se ha agregado a la cotización",
                                icon: 'error',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    <?php
                                    $ruta = ctrRuta::cargarRuta();
                                    ?>
                                    window.location = "<?php echo $ruta; ?>realizarCotizacion";
                                }
                            })
                        });
                    </script>
                <?php
                }
            } catch (GuzzleHttp\Exception\ClientException $e) {

                ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha agregado correctamente el nuevo registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>realizarCotizacion";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }


    // CONSULTAR TODOS LOS PRODUCTOS DEL CATALOGO
    public static function consultarProductosCatalogo()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'catalogo/consultarTodosLosProductos');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    // AGREGAR UN PRODUCTO EXISTENTE A LA COTIZACION
    public static function agregarProductoExistente()
    {
        if (isset($_POST["codigoProducto"]) && isset($_POST["cantidadProducto"])) {

            $codigo = $_POST["codigoProducto"];
            $cantidad = $_POST["cantidadProducto"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $respuesta = $cliente->post(
                    $base_uri . 'catalogo/consultarProductoExistente',
                    [
                        'body' => json_encode(
                            [
                                "codigo" => $codigo
                            ]
                        )
                    ]
                );

                $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

                array_push($respuesta_api["respuesta"][0][0], $cantidad);

                $_SESSION["cotizacionProductos"][count($_SESSION["cotizacionProductos"])] = $respuesta_api["respuesta"][0][0];

                $_SESSION["sumaSubtotal"] = 0;
                $_SESSION["isv"] = 0;
                $_SESSION["sumaTotal"] = 0;

                foreach ($_SESSION["cotizacionProductos"] as $key => $value) {
                    $_SESSION["sumaSubtotal"] = $_SESSION["sumaSubtotal"] + ($value["PRECIO"] * $value[0]);
                    $_SESSION["descuento"] = $_SESSION["sumaSubtotal"] * ($_SESSION["porcentaje_descuento"] / 100);
                    $_SESSION["isv"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) * 0.15;
                    $_SESSION["sumaTotal"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) + $_SESSION["isv"];
                }

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        <?php
                        $ruta = ctrRuta::cargarRuta();
                        ?>
                        window.location = "<?php echo $ruta; ?>realizarCotizacion";
                    })
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha agregado correctamente el producto a la cotización",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>realizarCotizacion";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }


    // ELIMINAR UN PRODUCTO DE LA COTIZACION
    public static function eliminarProducto()
    {
        if (isset($_POST["codigoProductoE"])) {

            $codigo = $_POST["codigoProductoE"];

            foreach ($_SESSION["cotizacionProductos"] as $key => $value) {

                if ($value["PK_COD_PRODUCTO"] ==  $codigo) {

                    unset($_SESSION["cotizacionProductos"][$key]);
                }
            }

            $_SESSION["sumaSubtotal"] = 0;
            $_SESSION["isv"] = 0;
            $_SESSION["sumaTotal"] = 0;

            foreach ($_SESSION["cotizacionProductos"] as $key => $value) {
                $_SESSION["sumaSubtotal"] = $_SESSION["sumaSubtotal"] + ($value["PRECIO"] * $value[0]);
                $_SESSION["descuento"] = $_SESSION["sumaSubtotal"] * ($_SESSION["porcentaje_descuento"] / 100);
                $_SESSION["isv"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) * 0.15;
                $_SESSION["sumaTotal"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) + $_SESSION["isv"];
            }

            ?>
            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>realizarCotizacion";
                })
            </script>
<?php

        }
    }
}
