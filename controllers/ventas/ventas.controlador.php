<?php

use GuzzleHttp\Client;

class ctrVentas
{

    public static function consultarCotizacionVenta($codigo)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get(
                $base_uri . 'cotizaciones/consultarCotizacionVenta',
                ['query' => ['codigo' => $codigo]]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }

    public static function consultarVenta($codigo)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get(
                $base_uri . 'ventas/consultarVenta',
                ['query' => ['codigo' => $codigo]]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }

    public static function consultarCotizacionesVenta()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'cotizaciones/consultarCotizacionesVenta');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }

    public static function productosMasVendidos()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'ventas/productosMasVendidos');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }

    public static function fechaMasVentas()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'ventas/fechaMasVentas');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }

    public static function consultarVentas()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'ventas/consultarVentas');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }

    static public function seleccionarCotizacion()
    {
        if (isset($_POST["idCotizacion"])) {

            $_SESSION["VENTA"]["idCotizacion"] = $_POST["idCotizacion"];
            $_SESSION["VENTA"]["codCliente"] = $_POST["codCliente"];
            $_SESSION["VENTA"]["nombreCliente"] = $_POST["nombreCliente"];

            $_SESSION["VENTA"]["idOrdenTrabajo"] = ctrVentas::consultarCotizacionVenta($_SESSION["VENTA"]["idCotizacion"]);

            $_SESSION["VENTA"]["detalleCotizacion"] = ctrOrdenDeTrabajo::consultarDetalleCotizacion($_SESSION["VENTA"]["idCotizacion"]);


            $_SESSION["VENTA"]["DETALLES"] = ctrCotizacion::consultarCotizacion($_SESSION["VENTA"]["idCotizacion"]);

?>
            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>registrarVenta";
                });
            </script>
            <?php

        }
    }

    static public function registrarVenta()
    {
        if (isset($_POST["tipoPago"])) {

            $cotizacion = $_SESSION["VENTA"]["idCotizacion"];
            $tipoPago = $_POST["tipoPago"];
            $metodoPago = $_POST["metodoPago"];

            if ($tipoPago === "CREDITO") {
                $metodoPago = 1;
            }

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

                $cliente->post(
                    $base_uri . 'ventas/agregarVentas',
                    [
                        'body' => json_encode([
                            "NO_FACTURA" => "",
                            "COD_COTIZACION" => $_SESSION["VENTA"]["idCotizacion"],
                            "COD_ORDEN_TRABAJO" => $_SESSION["VENTA"]["idOrdenTrabajo"]["PK_COD_ORDEN_TRABAJO"],
                            "ID_USUARIO_VENTA" => $_SESSION["ID_USUARIO"],
                            "TIPO_PAGO" => $tipoPago,
                            "COD_METODO_PAGO" => $metodoPago,
                            "SUBTOTAL" => $_SESSION["VENTA"]["DETALLES"]["SUBTOTAL"],
                            "DESCUENTO" => $_SESSION["VENTA"]["DETALLES"]["DESCUENTO"],
                            "ISV" => $_SESSION["VENTA"]["DETALLES"]["ISV"],
                            "TOTAL_PAGAR" => $_SESSION["VENTA"]["DETALLES"]["TOTAL_PAGAR"]
                        ])
                    ]
                );

                unset($_SESSION["VENTA"]);

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha registrado la factura correctamente",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.open(
                                    "tools/extension/tcpdf/examples/ventas/factura.php?codigo=" +
                                    <?php echo $cotizacion; ?>,
                                    "_black"
                                );
                                window.location = "<?php echo $ruta; ?>registrarVenta";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha registrado la factura correctamente",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>registrarVenta";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }


    public static function generarReportePorFechas()
    {

        if (isset($_POST["fechaInicio"]) && isset($_POST["fechaFinal"])) {

            $inicio = $_POST["fechaInicio"];
            $final = $_POST["fechaFinal"];

            ?>

            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    Swal.fire({
                        title: 'Generado!',
                        text: "Tu reporte se ha generado con exito!",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            <?php
                            $ruta = ctrRuta::cargarRuta();
                            ?>
                            window.open(
                                "tools/extension/tcpdf/examples/reportes/reporteVentas.php?inicio=<?php echo $inicio; ?>&final=<?php echo $final; ?>",
                                "_black"
                            );
                            window.location = "<?php echo $ruta; ?>reportes";
                        }
                    })
                });
            </script>

<?php

        }
    }

    public static function reportePorFechas($inicio, $final)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get(
                $base_uri . 'ventas/consultarVentaPorFecha',
                ['query' => ['INICIO' => $inicio, 'FINAL' => $final]]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }
}
