<?php

use GuzzleHttp\Client;

class ctrAccesosSistema
{

    // Comprobar acceso al módulo

    public static function comprobarAcceso($modulo)
    {

        $codigos = Accesos::codigos();
        $acceso = $codigos[$modulo];

        $base_uri = ApiConnection::Api();

        if (isset($modulo)) {

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $respuesta = $cliente->post(
                    $base_uri . 'sistema/consultarPermiso',
                    [
                        'body' => json_encode([
                            "usuario" => $_SESSION["ID_USUARIO"], 
                            "acceso" => $acceso]
                            )
                    ]
                );

                $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

                if (isset($respuesta_api["respuesta"][0][0])) {
                    return true;
                } else {
                    return false;
                }
            } catch (GuzzleHttp\Exception\ClientException $e) {
            }
        }
    }
}
