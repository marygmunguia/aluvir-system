<?php

use GuzzleHttp\Client;

class ctrRoles
{
    public static function consultarRoles()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'sistema/consultarRoles');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {

            print_r($e);
        }
    }

    public static function consultarAcciones()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'sistema/consultarAcciones');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {

            print_r($e);
        }
    }


    public static function consultarRolAcceso($codRol)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $_SESSION["TOKEN"]
                ]
            ]);

            $respuesta = $cliente->post(
                $base_uri . 'sistema/consultarRolAcceso',
                [
                    'body' => json_encode([
                        "codigoRol" => $codRol
                    ])
                ]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {

            print_r($e);
        }
    }


    public static function agregarNuevoRol()
    {
        if (isset($_POST["nombre"])) {
            if (isset($_POST["accesos"])) {
                $continuar = true;
                foreach ($_POST["accesos"] as $key => $value) {
                    if (isset($_POST["acciones" . $value])) {
                    } else {
                        $continuar = false;
?>
                        <script LANGUAGE="javascript">
                            $(document).ready(function() {
                                Swal.fire({
                                    title: 'Error!',
                                    text: "Debe de seleccionar al menos un acción de cada módulo seleccionado para crear un rol",
                                    icon: 'error',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Ok'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        <?php
                                        $ruta = ctrRuta::cargarRuta();
                                        ?>
                                        window.location = "<?php echo $ruta; ?>rolesSistema";
                                    }
                                })
                            });
                        </script>
                <?php
                    }
                }
            } else {
                $continuar = false;
                ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "Debe de seleccionar al menos un módulo para crear un rol",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>rolesSistema";
                            }
                        })
                    });
                </script>
                <?php
            }

            if ($continuar === true) {
                $nombre = $_POST["nombre"];

                $base_uri = ApiConnection::Api();

                try {

                    $cliente = new Client([
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => $_SESSION["TOKEN"]
                        ]
                    ]);

                    $cliente->post(
                        $base_uri . 'sistema/agregarNuevoRol',
                        [
                            'body' => json_encode([
                                "nombre" => $nombre,
                                "usuario" => $_SESSION["ID_USUARIO"]
                            ])
                        ]
                    );

                    // ULTIMO ROL AGREGADO
                    try {


                        $cliente = new Client([
                            'headers' => [
                                'Authorization' => $_SESSION["TOKEN"]
                            ]
                        ]);

                        $respuesta1 = $cliente->get(
                            $base_uri . 'sistema/ultimoRolAgregado'
                        );

                        $ultimoRol = json_decode(($respuesta1->getBody()->getContents()), true);

                        $ultimoRolAgregado =  $ultimoRol["respuesta"][0][0]["PK_COD_ROL"];

                        // AGREGAR ACCESOS
                        try {

                            foreach ($_POST["accesos"] as $key => $value) {

                                $cliente = new Client([
                                    'headers' => [
                                        'Content-Type' => 'application/json',
                                        'Authorization' => $_SESSION["TOKEN"]
                                    ]
                                ]);

                                $cliente->post(
                                    $base_uri . 'sistema/agregarAccesoAlRol',
                                    [
                                        'body' => json_encode([
                                            "rol" => $ultimoRolAgregado,
                                            "acceso" => $value
                                        ])
                                    ]
                                );

                                // AGREGAR ACCIONES DE LOS ACCESOS
                                try {

                                    foreach ($_POST["acciones" . $value] as $key1 => $value1) {

                                        try {


                                            $cliente = new Client([
                                                'headers' => [
                                                    'Authorization' => $_SESSION["TOKEN"]
                                                ]
                                            ]);

                                            $respuesta2 = $cliente->get(
                                                $base_uri . 'sistema/ultimoRolAccesoAgregado'
                                            );

                                            $ultimoCodigo = json_decode(($respuesta2->getBody()->getContents()), true);

                                            $ultimoCodigoAgregado =  $ultimoCodigo["respuesta"][0][0]["PK_COD"];
                                        } catch (GuzzleHttp\Exception\ClientException $e) {
                                            print_r($e);
                                        }

                                        $cliente = new Client([
                                            'headers' => [
                                                'Content-Type' => 'application/json',
                                                'Authorization' => $_SESSION["TOKEN"]
                                            ]
                                        ]);

                                        $cliente->post(
                                            $base_uri . 'sistema/agregarAccionAlAccesoDelRol',
                                            [
                                                'body' => json_encode([
                                                    "codigo" => $ultimoCodigoAgregado,
                                                    "accion" => $value1
                                                ])
                                            ]
                                        );
                                    }

                ?>
                                    <script LANGUAGE="javascript">
                                        $(document).ready(function() {
                                            Swal.fire({
                                                title: 'Guardado!',
                                                text: "Se ha agregado correctamente el nuevo registro",
                                                icon: 'success',
                                                showCancelButton: false,
                                                confirmButtonColor: '#3085d6',
                                                cancelButtonColor: '#d33',
                                                confirmButtonText: 'Ok'
                                            }).then((result) => {
                                                if (result.isConfirmed) {
                                                    <?php
                                                    $ruta = ctrRuta::cargarRuta();
                                                    ?>
                                                    window.location = "<?php echo $ruta; ?>rolesSistema";
                                                }
                                            })
                                        });
                                    </script>
<?php
                                } catch (GuzzleHttp\Exception\ClientException $e) {
                                    print_r($e);
                                }
                            }
                        } catch (GuzzleHttp\Exception\ClientException $e) {
                            print_r($e);
                        }
                    } catch (GuzzleHttp\Exception\ClientException $e) {

                        print_r($e);
                    }
                } catch (GuzzleHttp\Exception\ClientException $e) {

                    print_r($e);
                }
            }
        }
    }


    public static function eliminarRol(){
        
        if (isset($_POST["codigoB"])) {

            $nombre = $_POST["codigoB"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => [
                    'Content-Type' => 'application/json', 
                    'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

                $cliente->delete(
                    $base_uri . 'sistema/eliminarNuevoRol',
                    [
                        'body' => json_encode([
                            "nombre" => $nombre,
                            "usuario" => $_SESSION["ID_USUARIO"],
                        ])
                    ]
                );
?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Éxito!',
                            text: "Se ha eliminado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>rolesSistema";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha eliminado correctamente el registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>rolesSistema";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }
}
