<?php

use GuzzleHttp\Client;

class ctrLogin
{

    // Ingreso al sistema
    public static function ingresarSistema()
    {
        if (isset($_POST["usuario"])) {

            $usuario = $_POST["usuario"];
            $password = $_POST["password"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json']
                ]);

                $respuesta = $cliente->post(
                    $base_uri . 'usuarios/ingreso',
                    [
                        'body' => json_encode([
                            "nombre" => $usuario,
                            "clave" => $password,
                        ])
                    ]
                );

                $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

                $_SESSION["ID_USUARIO"] = $respuesta_api["respuesta"]["usuario"][0][0]["PK_ID_USUARIO"];
                $_SESSION["IMAGEN_USUARIO"] = $respuesta_api["respuesta"]["usuario"][0][0]["IMG_USUARIO"];
                $_SESSION["NOMBRE_USUARIO"] = $respuesta_api["respuesta"]["usuario"][0][0]["NOM_USUARIO"];
                $_SESSION["ESTADO_USUARIO"] = $respuesta_api["respuesta"]["usuario"][0][0]["EST_USUARIO"];
                $_SESSION["ID_EMPLEADO"] = $respuesta_api["respuesta"]["usuario"][0][0]["ID_EMPLEADO"];
                $_SESSION["TOKEN"] = $respuesta_api["respuesta"]["token"];

                $_SESSION["INGRESAR"] = 'ok';
                
                ctrCotizacion::ejecutarLimpiezaCotizacion();

?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Acceso permitido!',
                            text: "Bienvenido(a) al sistema",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Continuar'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>home";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

                echo '<br />
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-ban"></i> Error!</h5> Se ha presentado un error, usuario y/o contraseña incorrecta.
                </div>';

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Acceso NO permitido!',
                            text: "Se ha presentado un error, usuario y/o contraseña incorrecta.",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Volver'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>login";
                            }
                        })
                    });
                </script>
<?php

            }
        }
    }
}
