<?php

use GuzzleHttp\Client;

class ctrProveedores
{

    public static function consultarProveedoresReporte()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'proveedores/consultarProveedoresReporte');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }

    // Consultar todos los proveedores
    public static function consultarProveedores()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'proveedores/consultarProveedores');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    // Consultar una de las Categorias
    public static function consultarProveedor($codigo)
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->post(
                $base_uri . 'proveedores/consultarProveedor',
                [
                    'body' => json_encode([
                        "cod" => $codigo
                    ])
                ]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    // Agregar nuevo proveedor
    public static function agregarProveedor()
    {
        if (isset($_POST["rtn"])) {

            $rtn = $_POST["rtn"];
            $nombre = $_POST["nombre"];
            $direccion = $_POST["direccion"];
            $email = $_POST["email"];
            $telefono = $_POST["telefono"];
            $fax = $_POST["fax"];
            $celular = $_POST["celular"];
            $sitio = $_POST["sitio"];
            $asesor = $_POST["asesor"];
            $emaila = $_POST["emaila"];
            $cela = $_POST["cela"];


            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $respuesta = $cliente->post(
                    $base_uri . 'proveedores/agregarProveedor',
                    [
                        'body' => json_encode([
                            "rtn" => $rtn,
                            "nombre" => $nombre,
                            "direccion" => $direccion,
                            "correo" => $email,
                            "telefono" => $telefono,
                            "fax" => $fax,
                            "celular" => $celular,
                            "sitioweb" => $sitio,
                            "asesor" => $asesor,
                            "asesorcorreo" => $emaila,
                            "asesorcel" => $cela
                        ])
                    ]
                );
?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha agregado correctamente el nuevo registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>proveedores";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha agregado correctamente el nuevo Proveedor",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>proveedores";
                            }
                        })
                    });
                </script>
            <?php
                print_r($e);
            }
        }
    }



    // Actualizar Categoria
    public static function actualizarProveedor()
    {
        if (isset($_POST["codigoE"])) {

            $codigo = $_POST["codigoE"];
            $rtn = $_POST["rtnE"];
            $nombre = $_POST["nombreE"];
            $direccion = $_POST["direccionE"];
            $email = $_POST["emailE"];
            $telefono = $_POST["telefonoE"];
            $fax = $_POST["faxE"];
            $celular = $_POST["celularE"];
            $sitio = $_POST["sitioE"];
            $asesor = $_POST["asesorE"];
            $emaila = $_POST["emailAsesorE"];
            $cela = $_POST["celularAsesorE"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->put(
                    $base_uri . 'proveedores/actualizarProveedor',
                    [
                        'body' => json_encode([
                            "cod" => $codigo,
                            "rtn" => $rtn,
                            "nombre" => $nombre,
                            "direccion" => $direccion,
                            "correo" => $email,
                            "telefono" => $telefono,
                            "fax" => $fax,
                            "celular" => $celular,
                            "sitioweb" => $sitio,
                            "asesor" => $asesor,
                            "asesorcorreo" => $emaila,
                            "asesorcel" => $cela
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Cambio Exitoso!',
                            text: "Se ha actualizado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>proveedores";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha Actualizado correctamente",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>proveedores";
                            }
                        })
                    });
                </script>
            <?php
                print_r($e);
            }
        }
    }


    // Eliminar Categoria
    public static function eliminarProveedor()
    {
        if (isset($_POST["codigoB"])) {

            $codigo = $_POST["codigoB"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->delete(
                    $base_uri . 'proveedores/eliminarProveedor',
                    [
                        'body' => json_encode([
                            "cod" => $codigo
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Borrado Exitoso!',
                            text: "Se ha eliminado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>proveedores";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "No se ha eliminado correctamente el registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>proveedores";
                            }
                        })
                    });
                </script>
<?php
            }
        }
    }
}
