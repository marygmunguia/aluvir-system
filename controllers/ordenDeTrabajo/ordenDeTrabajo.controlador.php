<?php

use GuzzleHttp\Client;

class ctrOrdenDeTrabajo
{

    static public function seleccionarCotizacion()
    {
        if (isset($_POST["idCotizacion"])) {

            $_SESSION["ORDEN_TRABAJO"]["idCotizacion"] = $_POST["idCotizacion"];
            $_SESSION["ORDEN_TRABAJO"]["codCliente"] = $_POST["codCliente"];
            $_SESSION["ORDEN_TRABAJO"]["nombreCliente"] = $_POST["nombreCliente"];

            $_SESSION["ORDEN_TRABAJO"]["detalleCotizacion"] = ctrOrdenDeTrabajo::consultarDetalleCotizacion($_SESSION["ORDEN_TRABAJO"]["idCotizacion"]);

?>
            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>ordenDeTrabajo";
                });
            </script>
        <?php

        }
    }


    static public function seleccionarEmpleado()
    {
        if (isset($_POST["codEmpleado"])) {

            $_SESSION["ORDEN_TRABAJO"]["codEmpleado"] = $_POST["codEmpleado"];
            $_SESSION["ORDEN_TRABAJO"]["nombreEmpleado"] = $_POST["nombreEmpleado"];

        ?>
            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>ordenDeTrabajo";
                });
            </script>
        <?php

        }
    }


    public static function consultarDetalleCotizacion($cotizacion)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->post(
                $base_uri . 'cotizaciones/consultarDetallesCotizacion',
                [
                    'body' => json_encode([
                        "codigo" => $cotizacion
                    ])
                ]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    public static function consultarDetalleCotizacionImprimir($cotizacion)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Content-Type' => 'application/json']
            ]);

            $respuesta = $cliente->post(
                $base_uri . 'cotizaciones/consultarDetallesCotizacionImprimir',
                [
                    'body' => json_encode([
                        "codigo" => $cotizacion
                    ])
                ]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    public static function consultarMateriales()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get(
                $base_uri . 'materiales/consultarMateriales'
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    static public function cancelarOrdenDeTrabajo()
    {
        if (isset($_POST["Cancelar"])) {

            $_SESSION["ORDEN_TRABAJO"] = null;

        ?>
            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    Swal.fire({
                        title: 'Cancelada!',
                        text: "Se ha cancelado existosamente la orden de trabajo",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            <?php
                            $ruta = ctrRuta::cargarRuta();
                            ?>
                            window.location = "<?php echo $ruta; ?>ordenDeTrabajo";
                        }
                    })
                });
            </script>
<?php
        }
    }

    public function limpiarOrden()
    {
        $_SESSION["ORDEN_TRABAJO"] = null;
    }

    public function agregarOrden()
    {
        if (isset($_SESSION["ORDEN_TRABAJO"]["idCotizacion"]) && isset($_POST["fechaEntrega"]) && isset($_SESSION["ORDEN_TRABAJO"]["codEmpleado"])) {

            $fechaEntrega = $_POST["fechaEntrega"];
            $notas = $_POST["notas"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => [
                    'Content-Type' => 'application/json', 
                    'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

                $cliente->post(
                    $base_uri . 'ordenDeTrabajo/agregarOrdenDeTrabajo',
                    [
                        'body' => json_encode([
                            "EMPLEADO" => $_SESSION["ORDEN_TRABAJO"]["codEmpleado"],
                            "COTIZACION" => $_SESSION["ORDEN_TRABAJO"]["idCotizacion"],
                            "FECHA" => $fechaEntrega,
                            "DETALLE" => $notas,
                            "ESTADO" => "EN PROCESO",
                        ])
                    ]
                );

                ctrOrdenDeTrabajo::limpiarOrden();
?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha agregado correctamente el nuevo registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>ordenDeTrabajo";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha agregado correctamente el nuevo registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>ordenDeTrabajo";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }


}
