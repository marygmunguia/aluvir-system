<?php

use GuzzleHttp\Client;

class ctrCotizacion
{
    public static function ejecutarLimpiezaCotizacion()
    {
        $_SESSION["CLIENTE_COTIZACION"] = "";
        $_SESSION["NOM_CLIENTE_COTIZACION"] = "";
        $_SESSION["cotizacionProductos"] = array();
        $_SESSION["sumaSubtotal"] = 0;
        $_SESSION["porcentaje_descuento"] = 0;
        $_SESSION["descuento"] = 0;
        $_SESSION["isv"] = 0;
        $_SESSION["sumaTotal"] = 0;
    }

    public static function cancelarCotizacion()
    {
        if (isset($_POST["Cancelar"])) {

            ctrCotizacion::ejecutarLimpiezaCotizacion();

?>
            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    Swal.fire({
                        title: 'Cancelada!',
                        text: "Se ha cancelado existosamente la cotización",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            <?php
                            $ruta = ctrRuta::cargarRuta();
                            ?>
                            window.location = "<?php echo $ruta; ?>realizarCotizacion";
                        }
                    })
                });
            </script>
        <?php
        }
    }

    public static function calcularCotizacion()
    {
        if ($_SESSION["cotizacionProductos"] != null) {
            $_SESSION["sumaSubtotal"] = 0;
            $_SESSION["isv"] = 0;
            $_SESSION["sumaTotal"] = 0;

            foreach ($_SESSION["cotizacionProductos"] as $key => $value) {
                $_SESSION["sumaSubtotal"] = $_SESSION["sumaSubtotal"] + ($value["PRECIO"] * $value[0]);
                $_SESSION["descuento"] = $_SESSION["sumaSubtotal"] * ($_SESSION["porcentaje_descuento"] / 100);
                $_SESSION["isv"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) * 0.15;
                $_SESSION["sumaTotal"] = ($_SESSION["sumaSubtotal"] - $_SESSION["descuento"]) + $_SESSION["isv"];
            }
        }
    }


    public static function seleccionarCliente()
    {
        if (isset($_POST["codCliente"])) {

            $_SESSION["CLIENTE_COTIZACION"] = $_POST["codCliente"];

            $respuesta = ctrClientes::consultarClientes();
            foreach ($respuesta as $key => $value) {
                if ($value["PK_COD_CLIENTE"] == $_POST["codCliente"]) {
                    $_SESSION["NOM_CLIENTE_COTIZACION"] = $value["NOM_CLIENTE"] . " " . $value["APE_CLIENTE"];
                }
            }
        ?>
            <script LANGUAGE="javascript">
                $(document).ready(function() {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>realizarCotizacion";
                });
            </script>
            <?php
        }
    }

    public function guardarCotizacion()
    {
        if (isset($_POST["idCliente"])) {

            if (isset($_SESSION["CLIENTE_COTIZACION"]) && $_SESSION["CLIENTE_COTIZACION"] != null) {

                if (count($_SESSION["cotizacionProductos"]) > 0) {

                    ctrCotizacion::calcularCotizacion();

                    $base_uri = ApiConnection::Api();

                    try {

                        $cliente = new Client([
                            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                        ]);

                        $cliente->post(
                            $base_uri . 'cotizaciones/agregarCotizacion',
                            [
                                'body' => json_encode([
                                    "cliente" => $_SESSION["CLIENTE_COTIZACION"],
                                    "usuario" => $_SESSION["ID_USUARIO"],
                                    "subtotal" => $_SESSION["sumaSubtotal"],
                                    "descuento" => $_SESSION["descuento"],
                                    "isv" => $_SESSION["isv"],
                                    "total" => $_SESSION["sumaTotal"]
                                ])
                            ]
                        );

                        // Consultar cotizacion guardada
                        try {
                            $cliente = new Client([
                                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                            ]);

                            $respuesta = $cliente->get($base_uri . 'cotizaciones/consultarUltimaCotizacion');
                            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);
                            $cotizacion = $respuesta_api["respuesta"][0][0]["PK_COD_COTIZACION"];
                            //Guardar detalle de cotizacion
                            try {
                                foreach ($_SESSION["cotizacionProductos"] as $key => $value) {
                                    $cliente = new Client([
                                        'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                                    ]);

                                    $cliente->post(
                                        $base_uri . 'cotizaciones/agregarDetallesCotizacion',
                                        [
                                            'body' => json_encode([
                                                "cotizacion" => $cotizacion,
                                                "producto" => $value["PK_COD_PRODUCTO"],
                                                "cantidad" => $value[0],
                                                "precio" => $value["PRECIO"]
                                            ])
                                        ]
                                    );
                                }

                                ctrCotizacion::ejecutarLimpiezaCotizacion();

            ?>
                                <script LANGUAGE="javascript">
                                    $(document).ready(function() {
                                        Swal.fire({
                                            title: 'Guardado!',
                                            text: "Se ha relizado correctamente el registro de la cotización",
                                            icon: 'success',
                                            showCancelButton: false,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Ok'
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                <?php
                                                $ruta = ctrRuta::cargarRuta();
                                                ?>
                                                window.location = "<?php echo $ruta; ?>cotizaciones";
                                            }
                                        })
                                    });
                                </script>
                            <?php
                            } catch (GuzzleHttp\Exception\ClientException $e) {
                            ?>
                                <script LANGUAGE="javascript">
                                    $(document).ready(function() {
                                        Swal.fire({
                                            title: 'Error!',
                                            text: "Se ha relizado el registro de la cotización, pero no se registraron los productos de la misma",
                                            icon: 'error',
                                            showCancelButton: false,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Ok'
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                <?php
                                                $ruta = ctrRuta::cargarRuta();
                                                ?>
                                                window.location = "<?php echo $ruta; ?>realizarCotizacion";
                                            }
                                        })
                                    });
                                </script>
                        <?php
                            }
                        } catch (GuzzleHttp\Exception\ClientException $e) {
                        }
                    } catch (GuzzleHttp\Exception\ClientException $e) {
                        echo $e;

                        ?>
                        <script LANGUAGE="javascript">
                            $(document).ready(function() {
                                Swal.fire({
                                    title: 'Error!',
                                    text: "NO se ha agregado correctamente el registro de la cotización",
                                    icon: 'error',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Ok'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        <?php
                                        $ruta = ctrRuta::cargarRuta();
                                        ?>
                                        window.location = "<?php echo $ruta; ?>realizarCotizacion";
                                    }
                                })
                            });
                        </script>
                    <?php
                    }
                } else {
                    ?>
                    <script LANGUAGE="javascript">
                        $(document).ready(function() {
                            Swal.fire({
                                title: 'Error!',
                                text: "Debes escoger al menos un producto para realizar la cotización",
                                icon: 'error',
                                showCancelButton: false,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    <?php
                                    $ruta = ctrRuta::cargarRuta();
                                    ?>
                                    window.location = "<?php echo $ruta; ?>realizarCotizacion";
                                }
                            })
                        });
                    </script>
                <?php
                }
            } else {
                ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "Debes escoger un cliente para realizar la cotización",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>realizarCotizacion";
                            }
                        })
                    });
                </script>
<?php
            }
        }
    }



    public static function consultarCotizacionesExistentes()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'cotizaciones/consultarCotizaciones');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    public static function consultarCotizacionVenta($codigo)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'cotizaciones/consultarCotizacionVenta', ['query' => ['codigo' => $codigo]]);

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    public static function consultarCotizacion($codigo)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get(
                $base_uri . 'cotizaciones/consultarCotizacionImprimir',
                ['query' => ['codigo' => $codigo]]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
            print_r($e);
        }
    }
}
