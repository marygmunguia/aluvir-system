<?php

use GuzzleHttp\Client;

class ctrPerfil
{

    public function cambiarFotoPerfil()
    {

        if (isset($_POST["idUsuarioFoto"])) {

            $id = $_POST["idUsuarioFoto"];

            $rutaImg = $_POST["fotoActual"];

            if (isset($_FILES["imagenNueva"]["tmp_name"]) && !empty($_FILES["imagenNueva"]["tmp_name"])) {

                if (!empty($_POST["fotoActual"])) {

                    unlink($_POST["fotoActual"]);
                }


                if ($_FILES["imagenNueva"]["type"] == "image/png") {

                    $nombre = mt_rand(100, 999);

                    $rutaImg = "views/img/usuarios/" . $id . "_" . $nombre . ".png";

                    $foto = imagecreatefrompng($_FILES["imagenNueva"]["tmp_name"]);

                    imagepng($foto, $rutaImg);
                }

                if ($_FILES["imagenNueva"]["type"] == "image/jpeg") {

                    $nombre = mt_rand(100, 999);

                    $rutaImg = "views/img/usuarios/" . $id . "_" . $nombre . ".jpg";

                    $foto = imagecreatefromjpeg($_FILES["imagenNueva"]["tmp_name"]);

                    imagejpeg($foto, $rutaImg);
                }
            }
            
            $base_uri = ApiConnection::Api();

            try {

                $_SESSION["IMAGEN_USUARIO"] = $rutaImg;

                $cliente = new Client([
                    'headers' => [
                    'Content-Type' => 'application/json', 
                    'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

                $cliente->post(
                    $base_uri . 'usuarios/cambiarImagenUsuario',
                    [
                        'body' => json_encode([
                            "RUTA_IMAGEN" => $rutaImg,
                            "CODIGO" => $id
                        ])
                    ]
                );
?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha actializado tu imagen de perfil correctamente",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>perfil";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha actializado tu imagen de perfil correctamente",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>perfil";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }
}
