<?php

use GuzzleHttp\Client;

class ctrMateriales
{

    // Consultar todos los Materiales
    public static function consultarMateriales()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'materiales/consultarMateriales');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    public static function materialPorAgotarse()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'materiales/materialPorAgotarse');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    // Agregar nuevo Material
    public static function agregarMateriales()
    {
        if (isset($_POST["nombre"])) {

            $Cod_Barra = $_POST["Cod_Barra"];
            $nombre = $_POST["nombre"];
            $Detalle = $_POST["Detalle"];
            $tipo_categoria = $_POST["tipo_categoria"];
            $precioventa = $_POST["precioventa"];
            $exismin = $_POST["exismin"];
            $exismax = $_POST["exismax"];
            $eliminar =0;
            

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => [
                    'Content-Type' => 'application/json', 
                    'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

               $respuesta= $cliente->post(
                    $base_uri . 'materiales/agregarMaterial',
                    [
                        'body' => json_encode([
                            "CODIGO" => $Cod_Barra,
                            "NOMBRE" => $nombre,
                            "DETALLE" => $Detalle,
                            "CODCATEGORIA" => $tipo_categoria,
                            "PRECIO" => $precioventa,
                            "EXISTENCIA" => 0,
                            "MINIMO" => $exismin,
                            "MAXIMO" => $exismax
                        ])
                    ]
                );
                ?>
<script LANGUAGE="javascript">
$(document).ready(function() {
    Swal.fire({
        title: 'Guardado!',
        text: "Se ha agregado correctamente el nuevo Material",
        icon: 'success',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok'
    }).then((result) => {
        if (result.isConfirmed) {
            <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
            window.location = "<?php echo $ruta; ?>materiales";
        }
    })
});
</script>
<?php

            } catch (GuzzleHttp\Exception\ClientException $e) {
                ?>
<script LANGUAGE="javascript">
$(document).ready(function() {
    Swal.fire({
        title: 'Error!',
        text: "NO se ha agregado correctamente el nuevo Material",
        icon: 'error',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok'
    }).then((result) => {
        if (result.isConfirmed) {
            <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
            window.location = "<?php echo $ruta; ?>materiales";
        }
    })
});
</script>
<?php
            print_r($e);
            }
        }
    }


    // Actualizar Material
    public static function actualizarMateriales()
    {
        if (isset($_POST["nombreE"])) {

            $codigoE = $_POST["codigoE"];
            $codBarraE = $_POST["codBarraE"];
            $nombreE = $_POST["nombreE"];
            $detallesE = $_POST["detallesE"];
            $CodCategoriaE = $_POST["CodCategoriaE"];
            $precioVentaE = $_POST["precioVentaE"];
            $minexisE = $_POST["minexisE"];
            $maxexisE = $_POST["maxexisE"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->put(
                    $base_uri . 'materiales/actualizarMaterial',
                    [
                        'body' => json_encode([   
                            "COD" => $codigoE,       
                            "CODIGO" => $codBarraE,
                            "NOMBRE" => $nombreE,
                            "DETALLE" => $detallesE,
                            "CODCATEGORIA" => $CodCategoriaE,
                            "PRECIO" => $precioVentaE,                            
                            "MINIMO" => $minexisE,
                            "MAXIMO" => $maxexisE
                        ])
                    ]
                );
                ?>
                <script LANGUAGE="javascript">
                $(document).ready(function() {
                    Swal.fire({
                        title: 'Guardado!',
                        text: "Se ha Actualizado correctamente el Material",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            <?php
                                                $ruta = ctrRuta::cargarRuta();
                                                ?>
                            window.location = "<?php echo $ruta; ?>materiales";
                        }
                    })
                });
                </script>
                <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {
                ?>
                <script LANGUAGE="javascript">
                $(document).ready(function() {
                    Swal.fire({
                        title: 'Error!',
                        text: "NO se ha Actualizado correctamente",
                        icon: 'error',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            <?php
                                                $ruta = ctrRuta::cargarRuta();
                                                ?>
                            window.location = "<?php echo $ruta; ?>materiales";
                        }
                    })
                });
                </script>
                <?php
               // print_r($e);

            }
        }
    }



    // Eliminar Material
    public static function eliminarMateriales()
    {
        if (isset($_POST["codigoB"])) {

            $codigoB = $_POST["codigoB"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->delete(
                    $base_uri . 'materiales/eliminarMaterial',
                    [
                        'body' => json_encode([
                            "COD" => $codigoB
                        ])
                    ]
                );
            ?>
<script LANGUAGE="javascript">
$(document).ready(function() {
    Swal.fire({
        title: 'Borrado Exitoso!',
        text: "Se ha eliminado correctamente el registro",
        icon: 'success',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok'
    }).then((result) => {
        if (result.isConfirmed) {
            <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
            window.location = "<?php echo $ruta; ?>materiales";
        }
    })
});
</script>
<?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
<script LANGUAGE="javascript">
$(document).ready(function() {
    Swal.fire({
        title: 'Error!',
        text: "No se ha eliminado correctamente el registro",
        icon: 'error',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ok'
    }).then((result) => {
        if (result.isConfirmed) {
            <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
            window.location = "<?php echo $ruta; ?>materiales";
        }
    })
});
</script>
<?php

            }
        }
    }
}