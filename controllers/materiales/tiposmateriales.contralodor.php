<?php

use GuzzleHttp\Client;

class ctrIdentificacionesMaterial
{

    // Consultar todos los Tipos de materiales
    public static function consultarTipoCategoria()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'materiales/consultartipoCategoria');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];

        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    // Consultar un tipo de identificacion especifico
    public static function consultartipoCategoria1($codigo)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => [
                'Content-Type' => 'application/json', 
                'Authorization' => $_SESSION["TOKEN"]
                ]
            ]);


            $respuesta = $cliente->post($base_uri . 'categorias/consultarCategoria', 
            [
                'body' => json_encode([                   
                    "COD" => $codigo
                ])
            ]);

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }
}