<?php

use GuzzleHttp\Client;

class ctrRegistroInventario
{

    static public function ConsultarInventario()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'materiales/consultarMateriales');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    static public function registrarIngresoMaterial()
    {
        if (isset($_POST["codMaterial"])) {

            $codigo = $_POST["codMaterial"];
            $existencia = $_POST["cantidad"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => [
                    'Content-Type' => 'application/json', 
                    'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

                $cliente->post(
                    $base_uri . 'materiales/registrarIngresoMaterial',
                    [
                        'body' => json_encode([
                            "CODIGO" => $codigo,
                            "EXISTENCIA" => $existencia,
                            "USUARIO" => $_SESSION["ID_USUARIO"]
                        ])
                    ]
                );
?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha agregado correctamente el nuevo registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>registrarCompra";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha agregado correctamente el nuevo registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>registrarCompra";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }

}