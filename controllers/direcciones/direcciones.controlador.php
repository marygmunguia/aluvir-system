<?php

use GuzzleHttp\Client;

class ctrDirecciones
{

    // Consultar todas las Direcciones
    public static function consultarDirecciones()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'direcciones/consultarDirecciones');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    // Consultar una de las Direcciones
    public static function consultarDireccion($codigo)
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->post(
                $base_uri . 'direcciones/consultarDireccion',
                [
                    'body' => json_encode([
                        "COD" => $codigo
                    ])
                ]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    // Agregar nueva Direccion
    public static function agregarNuevaDireccion()
    {
        if (isset($_POST["cod_cliente"]) && isset($_POST["departamento"]) && isset($_POST["colonia"])) {

            $cod_cliente = $_POST["cod_cliente"];
            $departamento = $_POST["departamento"];
            $municipio = $_POST["municipio"];
            $ciudad = $_POST["ciudad"];
            $colonia = $_POST["colonia"];
            $detalle = $_POST["detalle"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->post(
                    $base_uri . 'direcciones/agregarNuevaDireccion',
                    [
                        'body' => json_encode([
                            "CODCLIENTE" => $cod_cliente,
                            "DEPARTAMENTO" => $departamento,
                            "MUNICIPIO" => $municipio,
                            "CIUDAD" => $ciudad,
                            "COLONIA" => $colonia,
                            "DETALLE" => $detalle
                        ])
                    ]
                );
?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha agregado correctamente el nuevo registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>direcciones";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire(
                            'Error',
                            'NO se ha agregado correctamente el nuevo registro',
                            'error'
                        )
                    });
                </script>
            <?php

            }
        }
    }



    // Actualizar Direccion
    public static function actualizarDireccion()
    {
        if (isset($_POST["codigoE"])) {

            $codigo = $_POST["codigoE"];
            $cod_cliente = $_POST["cod_clienteE"];
            $departamento = $_POST["departamentoE"];
            $municipio = $_POST["municipioE"];
            $ciudad = $_POST["ciudadE"];
            $colonia = $_POST["coloniaE"];
            $detalle = $_POST["detalleE"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->put(
                    $base_uri . 'direcciones/actualizarDireccion',
                    [
                        'body' => json_encode([
                            "COD" => $codigo,
                            "CODCLIENTE" => $cod_cliente,
                            "DEPARTAMENTO" => $departamento,
                            "MUNICIPIO" => $municipio,
                            "CIUDAD" => $ciudad,
                            "COLONIA" => $colonia,
                            "DETALLE" => $detalle
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Cambio Exitoso!',
                            text: "Se ha actualizado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>direcciones";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire(
                            'Error',
                            'NO se ha actualizado correctamente el registro',
                            'error'
                        )
                    });
                </script>
            <?php

            }
        }
    }


    // Eliminar Direccion
    public static function eliminarDireccion()
    {
        if (isset($_POST["codigoB"])) {

            $codigo = $_POST["codigoB"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->delete(
                    $base_uri . 'direcciones/eliminarDireccion',
                    [
                        'body' => json_encode([
                            "COD" => $codigo
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Borrado Exitoso!',
                            text: "Se ha eliminado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>direcciones";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire(
                            'Error',
                            'NO se ha eliminado correctamente el registro',
                            'error'
                        )
                    });
                </script>
<?php

            }
        }
    }
}
