<?php

use GuzzleHttp\Client;

class ctrMetodospago
{

    // Consultar todas los Metodos de Pago
    public static function consultarMetodospago()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'metodospago/consultarMetodospago');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    // Consultar solo un Metodo de Pago
    public static function consultarMetodopago($codigo)
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->post(
                $base_uri . 'metodospago/consultarMetodopago',
                [
                    'body' => json_encode([
                        "PK_COD_METODO_PAGO" => $codigo
                    ])
                ]
            );

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    // Agregar Metodo de Pago
    public static function agregarMetodopago()
    {
        if (isset($_POST["nombre"])) {

            $nombre = $_POST["nombre"];
            $detalle = $_POST["detalle"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->post(
                    $base_uri . 'metodospago/agregarMetodopago',
                    [
                        'body' => json_encode([
                            "NOMBRE" => $nombre,
                            "DETALLE" => $detalle
                        ])
                    ]
                );
?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha agregado correctamente el nuevo registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>metodosPago";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire(
                            'Error',
                            'NO se ha agregado correctamente el nuevo registro',
                            'error'
                        )
                    });
                </script>
            <?php

            }
        }
    }



    // Actualizar Metodo de Pago
    public static function actualizarMetodopago()
    {
        if (isset($_POST["codigoE"])) {

            $codigo = $_POST["codigoE"];
            $nombre = $_POST["nombreE"];
            $detalle = $_POST["detalleE"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->put(
                    $base_uri . 'metodospago/actualizarMetodopago',
                    [
                        'body' => json_encode([
                            "COD" => $codigo,
                            "NOMBRE" => $nombre,
                            "DETALLE" => $detalle
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Cambio Exitoso!',
                            text: "Se ha actualizado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>metodosPago";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire(
                            'Error',
                            'NO se ha actualizado correctamente el registro',
                            'error'
                        )
                    });
                </script>
            <?php

            }
        }
    }


    // Eliminar Metodo de pago
    public static function eliminarMetodopago()
    {
        if (isset($_POST["codigoB"])) {

            $codigo = $_POST["codigoB"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->delete(
                    $base_uri . 'metodospago/eliminarMetodopago',
                    [
                        'body' => json_encode([
                            "COD" => $codigo
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Borrado Exitoso!',
                            text: "Se ha eliminado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>metodosPago";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire(
                            'Error',
                            'NO se ha eliminado correctamente el registro',
                            'error'
                        )
                    });
                </script>
<?php

            }
        }
    }
}
