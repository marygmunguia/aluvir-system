<?php

use GuzzleHttp\Client;

class ctrIdentificaciones
{

    // Consultar todos los Tipos de Identificaciones
    public static function consultarTiposIdentificaciones()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'clientes/consultarTiposIdentificaciones');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];

        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    // Consultar un tipo de identificacion especifico
    public static function consultarTipoIdentificacion($codigo)
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'clientes/consultarTipoIdentificacion', 
            [
                'query' => ['codigo' => $codigo]
            ]);

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0][0];
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }
}