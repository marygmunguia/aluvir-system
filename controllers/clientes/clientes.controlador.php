<?php

use GuzzleHttp\Client;

class ctrClientes
{

    // Consultar todas las Clientes
    public static function consultarClientes()
    {

        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client([
                'headers' => ['Authorization' => $_SESSION["TOKEN"]]
            ]);

            $respuesta = $cliente->get($base_uri . 'clientes/consultarClientes');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];

        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }

    public static function consultarClientesReporte()
    {
        $base_uri = ApiConnection::Api();

        try {

            $cliente = new Client();

            $respuesta = $cliente->get($base_uri . 'clientes/consultarClientesReporte');

            $respuesta_api = json_decode(($respuesta->getBody()->getContents()), true);

            return $respuesta_api["respuesta"][0];
            
        } catch (GuzzleHttp\Exception\ClientException $e) {
        }
    }


    // Agregar nuevo Cliente
    public static function agregarNuevoCliente()
    {
        if (isset($_POST["nombre"]) && isset($_POST["apellido"])) {

            $tipo_identificacion = $_POST["tipo_identificacion"];
            $no_identificacion = $_POST["no_identificacion"];
            $nombre = ucwords(strtolower($_POST["nombre"]));
            $apellido = ucwords(strtolower($_POST["apellido"]));
            $correo = $_POST["correo"];
            $telefono = $_POST["telefono"];
            $celular = $_POST["celular"];
            $credito = $_POST["credito"];

            if (isset($_POST["limite_credito"])) {
                $limite_credito = $_POST["limite_credito"];
            }else{
                $limite_credito = 0;
            }
            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => [
                    'Content-Type' => 'application/json', 
                    'Authorization' => $_SESSION["TOKEN"]
                    ]
                ]);

                $cliente->post(
                    $base_uri . 'clientes/agregarClientes',
                    [
                        'body' => json_encode([
                            "tipoIdentificacion" => $tipo_identificacion,
                            "numeroIdentificacion" => $no_identificacion,
                            "nombre" => $nombre,
                            "apellido" => $apellido,
                            "email" => $correo,
                            "telefono" => $telefono,
                            "celular" => $celular,
                            "credito" => $credito,
                            "limiteCredito" => $limite_credito
                        ])
                    ]
                );
?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Guardado!',
                            text: "Se ha agregado correctamente el nuevo registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>clientes";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha agregado correctamente el nuevo registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>clientes";
                            }
                        })
                    });
                </script>
            <?php
            }
        }
    }


    // Actualizar Cliente
    public static function actualizarCliente()
    {
        if (isset($_POST["nombreE"]) && isset($_POST["apellidoE"])) {

            $codigo = $_POST["codigoE"];
            $tipo_identificacion = $_POST["tipo_identificacionE"];
            $no_identificacion = $_POST["no_identificacionE"];
            $nombre = ucwords(strtolower($_POST["nombreE"]));
            $apellido = ucwords(strtolower($_POST["apellidoE"]));
            $correo = $_POST["correoE"];
            $telefono = $_POST["telefonoE"];
            $celular = $_POST["celularE"];
            $credito = $_POST["creditoE"];
            $limite_credito = $_POST["limite_creditoE"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->put(
                    $base_uri . 'clientes/actualizarClientes',
                    [
                        'body' => json_encode([
                            "codigo" => $codigo,
                            "tipoIdentificacion" => $tipo_identificacion,
                            "numeroIdentificacion" => $no_identificacion,
                            "nombre" => $nombre,
                            "apellido" => $apellido,
                            "email" => $correo,
                            "telefono" => $telefono,
                            "celular" => $celular,
                            "credito" => $credito,
                            "limiteCredito" => $limite_credito
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Exito!',
                            text: "Se ha actualizado correctamente el nuevo registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>clientes";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "NO se ha actualizado correctamente el nuevo registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>clientes";
                            }
                        })
                    });
                </script>
            <?php

            }
        }
    }



    // Eliminar Cliente
    public static function eliminarCliente()
    {
        if (isset($_POST["codigoB"])) {

            $codigo = $_POST["codigoB"];

            $base_uri = ApiConnection::Api();

            try {

                $cliente = new Client([
                    'headers' => ['Content-Type' => 'application/json', 'Authorization' => $_SESSION["TOKEN"]]
                ]);

                $cliente->delete(
                    $base_uri . 'clientes/eliminarClientes',
                    [
                        'body' => json_encode([
                            "codigo" => $codigo
                        ])
                    ]
                );
            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Borrado Exitoso!',
                            text: "Se ha eliminado correctamente el registro",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>clientes";
                            }
                        })
                    });
                </script>
            <?php

            } catch (GuzzleHttp\Exception\ClientException $e) {

            ?>
                <script LANGUAGE="javascript">
                    $(document).ready(function() {
                        Swal.fire({
                            title: 'Error!',
                            text: "No se ha eliminado correctamente el registro",
                            icon: 'error',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                <?php
                                $ruta = ctrRuta::cargarRuta();
                                ?>
                                window.location = "<?php echo $ruta; ?>clientes";
                            }
                        })
                    });
                </script>
<?php

            }
        }
    }
}
