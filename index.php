<?php

// autoload
require_once "./vendor/autoload.php";

// controladores
require_once "./controllers/seguridad/login.controlador.php";
require_once "./controllers/sistema/ruta.controlador.php";
require_once "./controllers/sistema/plantilla.controlador.php";
require_once "./controllers/sistema/api.controlador.php";
require_once "./controllers/categorias/categorias.controlador.php";
require_once "./controllers/seguridad/accesos.controlador.php";
require_once "./controllers/clientes/clientes.controlador.php";
require_once "./controllers/clientes/tiposIdentificacion.controlador.php";
require_once "./controllers/usuarios/usuarios.controlador.php";
require_once "./controllers/direcciones/direcciones.controlador.php";
require_once "./controllers/catalogo/catalogo.controlador.php";
require_once "./controllers/cotizaciones/cotizaciones.controlador.php";
require_once "./controllers/sistema/perfil.controlador.php";
require_once "./controllers/materiales/materiales.controlador.php";
require_once "./controllers/materiales/tiposmateriales.contralodor.php";
require_once "./controllers/seguridad/roles.controlador.php";
require_once "./controllers/metodosPago/metodosPago.controlador.php";
require_once "./controllers/inventario/registrar.controlador.php";
require_once "./controllers/compras/compras.controlador.php";
require_once "./controllers/ordenDeTrabajo/ordenDeTrabajo.controlador.php";
require_once "./controllers/ventas/ventas.controlador.php";
require_once "./controllers/proveedores/proveedores.controlador.php";

// tools
require_once "./tools/accesos.tools.php";
require_once "./tools/generar.tools.php";

// Cargar plantilla
$plantilla = new ctrPlantilla;
$plantilla -> cargarPlantilla();