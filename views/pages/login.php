<div class="login-box">
  <div class="login-logo">
    <a class="titulo-1" href="#"><b>ALUVIR</b> SYSTEM</a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Ingresa tu usuario y contraseña para acceder al sistema</p>
      <form method="post">
        <div class="input-group mb-3">
          <input type="text" name="usuario" class="form-control" placeholder="Usuario" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Contraseña" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-1 btn-block">Acceder</button>
            <?php
            
            $ingreso = new ctrLogin;
            $ingreso -> ingresarSistema();

            ?>
          </div>
        </div>
      </form><br />
      <p class="mb-1">
        <a href="olvidarClave" class="link-1">¿Haz olvidado tu contraseña?</a>
      </p>
    </div>
  </div>
</div>