<?php
$permiso = ctrAccesosSistema::comprobarAcceso("usuarios");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>GESTIÓN DE EMPLEADOS</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Empleados</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="card">
                    <form method="post">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-9">
                            Registrar un nuevo empleado al sistema
                            </div>
                            <div class="col-3">
                            <input type="submit" class="btn btn-primary btn-block" value="Guardar">
                            </div>
                        </div>
                    </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-4">
                                    <h4>Datos Generales</h4><br>
                                    <div class="form-group">
                                        <label for="nombre">Nombre:</label>
                                        <input type="text" name="nombre" id="nombre" class="form-control" required
                                        pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido" maxlength="50">
                                    </div>
                                    <div class="form-group">
                                        <label for="apellido">Apellido:</label>
                                        <input type="text" name="apellido" id="apellido" class="form-control" 
                                        required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido" maxlength="50">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Fecha de nacimiento:</label>
                                        <input type="date" max="<?php date("Y-m-d"); ?>" name="fec_nacimiento" id="fec_nacimiento" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Sexo:</label>
                                        <select name="sexo" id="sexo" class="form-control" required>
                                            <option value="M">Masculino</option>
                                            <option value="F">Femenino</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <h4>Datos de Contacto</h4><br>
                                    <div class="form-group">
                                        <label for="">Dirección:</label>
                                        <textarea name="direccion" id="direccion" maxlength="400" cols="3" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Número de teléfono fijo:</label>
                                        <input type="text" name="telefono" placeholder="0000-0000" pattern="^[0-9-]+$" title="Debe de ingresar un número de teléfono válido" id="telefono" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Número de teléfono móvil:</label>
                                        <input type="text" name="celular" placeholder="0000-0000" pattern="^[0-9-]+$" title="Debe de ingresar un número de móvil válido" id="celular" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Correo Eléctronico:</label>
                                        <input type="email" name="correo" id="correo" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <h4>Permisos de Usuario</h4><br>
                                    <div class="form-group">
                                        <label>Selecciona los módulos a los que tendra acceso el usuario:</label><br>
                                        <?php
                                        $accesos = ctrUsuarios::consultarAccesos();
                                        foreach ($accesos as $key => $value) {
                                        ?>
                                            <input type="checkbox" name="accesos[]" value="<?php echo $value["PK_COD_ACCESO"]; ?>">
                                            <label><?php echo $value["NOM_ACCESO"]; ?></label>
                                            <br>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <p class="text-danger">Importante: Las credenciales será definidas por el sistema y enviadas al correo del usuario.</p>
                        </div>
                        <?php
                        
                        $guardar = new ctrUsuarios();
                        $guardar -> guardarUsuario();

                        ?>
                    </form>
                </div>
            </div>
        </section>
    </div>
<?php
} else {
?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
