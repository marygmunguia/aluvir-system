<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>GESTIÓN DE PROVEEDORES</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Proveedores</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">

        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#agregarProveedor">
                        Agregar Nuevo proveedor
                    </button>
                </div>
                <div class="card-body">
                    <table id="registros" style="width: 100%;" class="table responsive table-bordered table-hover TB">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>RTN</th>
                                <th>Nombre</th>
                                <th>Dirección</th>
                                <th>Correo Electrónico</th>
                                <th>Telefono</th>
                                <th>Fax</th>
                                <th>Celular</th>
                                <th>Sitio web</th>
                                <th>Asesor</th>
                                <th>Correo Electrónico del Asesor</th>
                                <th>Celular del Asesor</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $respuesta = ctrProveedores::consultarProveedores();

                            foreach ($respuesta as $key => $value) {

                            ?>
                                <tr>
                                    <td><?php echo $value["PK_COD_PROVEEDOR"]; ?></td>
                                    <td><?php echo $value["RTN_PROVEEDOR"]; ?></td>
                                    <td><?php echo $value["NOM_PROVEEDOR"];  ?></td>
                                    <td><?php echo $value["DIRECCION"]; ?></td>
                                    <td><?php echo $value["EMAIL"]; ?></td>
                                    <td><?php echo $value["TELEFONO"];  ?></td>
                                    <td><?php echo $value["FAX"]; ?></td>
                                    <td><?php echo $value["CELULAR"]; ?></td>
                                    <td><?php echo $value["SITIO_WEB"];  ?></td>
                                    <td><?php echo $value["ASESOR_VENTAS"]; ?></td>
                                    <td><?php echo $value["EMAIL_ASESOR_VENTAS"]; ?></td>
                                    <td><?php echo $value["CELULAR_ASESOR_VENTAS"];  ?></td>
                                    <td style="width: 5%;">
                                        <center>
                                            <button cod="<?php echo $value["PK_COD_PROVEEDOR"]; ?>" rtn="<?php echo $value["RTN_PROVEEDOR"]; ?>" nombre="<?php echo $value["NOM_PROVEEDOR"]; ?>" direccion="<?php echo $value["DIRECCION"]; ?>" email="<?php echo $value["EMAIL"]; ?>" telefono="<?php echo $value["TELEFONO"]; ?>" fax="<?php echo $value["FAX"]; ?>" celular="<?php echo $value["CELULAR"]; ?>" sitio="<?php echo $value["SITIO_WEB"]; ?>" asesor="<?php echo $value["ASESOR_VENTAS"]; ?>" emaila="<?php echo $value["EMAIL_ASESOR_VENTAS"]; ?>" cela="<?php echo $value["CELULAR_ASESOR_VENTAS"]; ?>" type="button" class="btn btn-warning editarProveedor" data-toggle="modal" data-target="#actualizarProveedor">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </center>
                                    </td>
                                    <td style="width: 5%;">
                                        <center>
                                            <button cod="<?php echo $value["PK_COD_PROVEEDOR"]; ?>" type="button" class="btn btn-danger eliminarProveedor" data-toggle="modal" data-target="#eliminarProveedor">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </center>
                                    </td>
                                </tr>

                            <?php
                            }
                            ?>
                    </table>
                </div>

            </div>

        </div>

    </section>
</div>



<!-- Formulario de Nuevo Proveedor -->
<div class="modal fade" id="agregarProveedor">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Nuevo proveedor</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="rtn">RTN:</label>
                                <input type="text" class="form-control" name="rtn" id="rtn" maxlength="20" pattern="^[0-9-]+$" title="Debe de ingresar un RTN válido">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="nombre">Nombre del Proveedor:</label>
                                <input type="text" class="form-control" name="nombre" id="nombre" maxlength="50" required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="direccion">Direccion del Proveedor:</label>
                                <textarea class="form-control" name="direccion" id="direccion" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="sitio">Sitio Web:</label>
                                <input type="text" name="sitio" class="form-control" id="sitio" maxlength="40">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="telefono">Teléfono:</label>
                                <input type="text" class="form-control" name="telefono" id="telefono" maxlength="20" pattern="^[0-9-]+$" title="Debe de ingresar un número de teléfono válido">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="fax">FAX</label>
                                <input type="text" class="form-control" name="fax" id="fax" maxlength="20" pattern="^[0-9-]+$" title="Debe de ingresar un número de fax válido">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="celular">Celular:</label>
                                <input type="text" class="form-control" name="celular" id="celular" maxlength="20" pattern="^[0-9-]+$" title="Debe de ingresar un número de celular válido">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="asesor">Asesor:</label>
                                <input type="text" name="asesor" class="form-control" id="asesor" pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido" maxlength="50">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="emaila">Email Asesor:</label>
                                <input type="email" name="emaila" class="form-control" id="emaila">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="cela">Celular Asesor:</label>
                                <input type="text" name="cela" class="form-control" id="cela" maxlength="20" pattern="^[0-9-]+$" title="Debe de ingresar un número de celular válido">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar cambios">
                </div>
                <?php

                $agregar = new ctrProveedores();
                $agregar->agregarProveedor();

                ?>
            </form>
        </div>

    </div>

</div>

<!-- Formulario de actualizar Proveedor -->
<div class="modal fade" id="actualizarProveedor">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Actualizar Proveedor</h4>
                </div>
                <div class="modal-body">
                <input type="hidden" id="codigoE" name="codigoE">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="rtnE">RTN:</label>
                                <input type="text" class="form-control" name="rtnE" id="rtnE" maxlength="20" pattern="^[0-9-]+$" title="Debe de ingresar un RTN válido">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="nombreE">Nombre del Proveedor:</label>
                                <input type="text" class="form-control" name="nombreE" id="nombreE" 
                                pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido" maxlength="50" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="direccionE">Direccion del Proveedor:</label>
                                <textarea class="form-control" name="direccionE" id="direccionE" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="emailE">Email:</label>
                                <input type="email" class="form-control" name="emailE" id="emailE">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="sitioE">Sitio Web:</label>
                                <input type="text" maxlength="50" name="sitioE" class="form-control" id="sitioE">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="telefonoE">Teléfono:</label>
                                <input type="text" class="form-control" name="telefonoE" id="telefonoE" maxlength="15" pattern="^[0-9-]+$" title="Debe de ingresar un número de teléfono válido">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="faxE">Fax:</label>
                                <input type="text" class="form-control" name="faxE" id="faxE" maxlength="15" pattern="^[0-9-]+$" title="Debe de ingresar un número de fax válido">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="celularE">Celular:</label>
                                <input type="text" class="form-control" name="celularE" id="celularE" maxlength="15" pattern="^[0-9-]+$" title="Debe de ingresar un número de celular válido">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="asesorE">Asesor:</label>
                                <input type="text" name="asesorE" class="form-control" maxlength="50"  pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido" id="asesorE">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="emailAsesorE">Email Asesor:</label>
                                <input type="email" name="emailAsesorE" class="form-control" id="emailAsesorE">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="celularAsesorE">Celular Asesor:</label>
                                <input type="text" name="celularAsesorE" pattern="^[0-9-]+$" maxlength="15" title="Debe de ingresar un número de celular válido" class="form-control" id="celularAsesorE">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-warning" value="Aplicar cambios">
                </div>
                <?php

                $actualizar = new ctrProveedores();
                $actualizar->actualizarProveedor();

                ?>
            </form>
        </div>
    </div>
</div>


<!-- Formulario de eliminar Proveedor -->
<div class="modal fade" id="eliminarProveedor">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Proveedor</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>¿Esta seguro que desea eliminar el registro?</p>
                        <input type="hidden" id="codigoB" name="codigoB">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </div>
                <?php

                $eliminar = new ctrProveedores;
                $eliminar->eliminarProveedor();

                ?>
            </form>
        </div>
    </div>
</div>





<script>
    // Editar Proveedor

    $("#registros").on("click", ".editarProveedor", function() {
        $("#codigoE").val($(this).attr("cod"));
        $("#rtnE").val($(this).attr("rtn"));
        $("#nombreE").val($(this).attr("nombre"));
        $("#direccionE").val($(this).attr("direccion"));
        $("#emailE").val($(this).attr("email"));
        $("#telefonoE").val($(this).attr("telefono"));
        $("#faxE").val($(this).attr("fax"));
        $("#celularE").val($(this).attr("celular"));
        $("#sitioE").val($(this).attr("sitio"));
        $("#asesorE").val($(this).attr("asesor"));
        $("#emailAsesorE").val($(this).attr("emaila"));
        $("#celularAsesorE").val($(this).attr("cela"));
    });

    // Eliminar Proveedor

    $("#registros").on("click", ".eliminarProveedor", function() {
        $("#codigoB").val($(this).attr("cod"));
    });
</script>