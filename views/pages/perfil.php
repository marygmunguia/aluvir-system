<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>MI PERFIL</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Perfil</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-widget widget-user">

                    <div class="widget-user-header bg-dark">
                        <h3 class="widget-user-username"><?php echo $_SESSION['NOMBRE_USUARIO'] ?></h3>
                    </div>
                    <div class="widget-user-image">
                        <img class="img-circle" src="<?php echo $ruta, $_SESSION["IMAGEN_USUARIO"]; ?>" alt="User Avatar">
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">


                                <center>
                                    <br />
                                    <button class="btn btn-primary btn-block" style="color: #fff;" data-toggle="modal" data-target="#CambiarFoto">Cambiar fotografía</button>
                                    <br /><br />
                                </center>

                            </div>

                            <div class="col-sm-4 border-right">
                                <center>
                                    <br />
                                    <button class="btn btn-warning btn-block" style="color: #000;" data-toggle="modal" data-target="#CambiarClaveAcceso">
                                        Cambiar contraseña</button>
                                    <br /><br />
                                </center>

                            </div>

                            <div class="col-sm-4">

                                <center>
                                    <br />
                                    <button class="btn btn-danger btn-block" data-toggle="modal" data-target="#cerrarSesion">
                                        Cerrar Sesión en Sistema</button>
                                    <br /><br />
                                </center>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" rol="dialog" id="cerrarSesion">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">
                <h3>Cerrar Sesión</h3>
            </div>

            <div class="modal-body">

                <div class="box-body">

                    <div class="form-group">
                        <label for="">¿Deseas cerrar sesión definitivamente?</label>

                    </div>
                </div>
            </div>

            <div class="modal-footer">

                <a href="cerrarSession" class="btn btn-success"> Aceptar </a>

                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" rol="dialog" id="CambiarClaveAcceso">

    <div class="modal-dialog">

        <div class="modal-content">

            <form method="post" role="form">

                <div class="modal-header">
                    <h3>Cambiar contreseña</h3>
                </div>

                <div class="modal-body">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña Actual:</label>
                            <input type="password" class="form-control" id="claveActual" name="claveActual" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña Nueva:</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Repetir Contraseña Nueva:</label>
                            <input type="password" class="form-control" name="password1" id="password1" placeholder="">
                        </div>

                    </div>
                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-1" onclick="return ValidarPassword();"> Guardar cambios</button>

                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancelar</button>
                </div>
                <?php

                $cambiar = new ctrUsuarios();
                $cambiar -> CambiarPassword();

                ?>
            </form>
        </div>
    </div>
</div>


<!-- CAMBIAR FOTOGRAFÍA DE PERFIL -->
<div class="modal fade" rol="dialog" id="CambiarFoto">

    <div class="modal-dialog">

        <div class="modal-content">

            <form method="post" enctype="multipart/form-data">

                <div class="modal-header">
                    <h3>Cambiar Imagen de Perfil</h3>
                </div>

                <div class="modal-body">

                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputPassword1">Seleccione la imagen en <span style="color: red;">formato .jpg o .png</span></label>
                            <div class="form-group">
                                <input type="hidden" name="idUsuarioFoto" value="<?php echo $_SESSION["ID_USUARIO"]; ?>" />
                                <input type="file" class="form-control-file border" name="imagenNueva" id="imagenNueva" required>
                                <input type="hidden" name="fotoActual" value="<?php echo $_SESSION["IMAGEN_USUARIO"]; ?>" />
                                <br /><br />
                                <img src="<?php echo $_SESSION["IMAGEN_USUARIO"]; ?>" alt="" class="previsualizar" width="150px" height="150px">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-1"> Cambiar </button>

                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancelar</button>
                </div>
                <?php

                $cambiar = new ctrPerfil();
                $cambiar->cambiarFotoPerfil();

                ?>
            </form>
        </div>
    </div>
</div>

<script>
    function ValidarPassword() {

        pass1 = document.getElementById('password');
        pass2 = document.getElementById('password1');

        // Verificamos si las constraseñas no coinciden
        if (pass1.value != pass2.value) {

            // Si las constraseñas no coinciden mostramos un mensaje
            swal({
                titltype: "error",
                title: "¡ERROR!",
                text: "LAS CLAVES NO COINCIDEN",
                showConfirmButtom: true,
                confirmButtomText: "Cerrar"
            }).then((result) => {
                if (result.value) {
                    window.location = "<?php echo $ruta; ?>perfil";
                }
            });

            return false;

        } else {

            return true;
        }

    }

    $("#imagenNueva").change(function() {

        var imagen = this.files[0];

        if (imagen["type"] != "image/jpeg" && imagen["type"] != "image/png") {

            $(".imagenNueva").val("");

            swal({
                title: "Error al subir la imagen",
                text: "¡La imagen debe estar en formato JPG o PNG!",
                type: "error",
                confirmButtonText: "¡Cerrar!"
            });

        } else if (imagen["size"] > 2000000) {

            $(".imagenNueva").val("");

            swal({
                title: "Error al subir la imagen",
                text: "¡La imagen no debe pesar más de 2MB!",
                type: "error",
                confirmButtonText: "¡Cerrar!"
            });

        } else {

            var datosImagen = new FileReader;
            datosImagen.readAsDataURL(imagen);

            $(datosImagen).on("load", function(event) {

                var rutaImagen = event.target.result;

                $(".previsualizar").attr("src", rutaImagen);

            })

        }
    })
</script>