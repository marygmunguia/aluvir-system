<?php
$permiso = ctrAccesosSistema::comprobarAcceso("inventario");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>ROLES DE SISTEMA</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Roles</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevoRol">
                            Agregar Nuevo Rol
                        </button>
                    </div>
                    <div class="card-body">
                        <table id="registros" style="width: 100%;" class="table responsive table-bordered table-hover TB">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Módulos Permitidos</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $respuesta = ctrRoles::consultarRoles();
                                foreach ($respuesta as $key => $value) {
                                ?>
                                    <tr>
                                        <td><?php echo $value["PK_COD_ROL"]; ?></td>
                                        <td><?php echo $value["NOM_ROL"]; ?></td>
                                        <td>
                                            <?php
                                            $accesos = ctrRoles::consultarRolAcceso($value["PK_COD_ROL"]);
                                            foreach ($accesos as $clave => $valor) {
                                                echo $valor["NOM_ACCESO"];
                                            ?>
                                                <br>
                                            <?php } ?>
                                        </td>
                                        <td style="width: 5%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_ROL"]; ?>" type="button" class="btn btn-danger eliminarCategoria" data-toggle="modal" data-target="#eliminarRol">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                        </table>
                    </div>

                </div>

            </div>

        </section>
    </div>

<?php
} else {
?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>


<!-- Formulario de Nuevo Rol -->
<div class="modal fade" id="nuevoRol">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Nuevo Rol</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nombre" class="text-danger"> * Nombre:</label>
                        <input type="text" class="form-control" maxlength="20" name="nombre" id="nombre" required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido">
                    </div>
                    <div class="form-group">
                        <label>Selecciona los módulos a los que tendra acceso este rol:</label><br>
                        <label class="text-danger"><input type="checkbox" id="MarcarTodos" /> ACCESO COMPLETO</label><br>
                        <?php
                        $accesos = ctrUsuarios::consultarAccesos();
                        foreach ($accesos as $key => $value) {
                        ?>
                            <input type="checkbox" name="accesos[]" class="campos" value="<?php echo $value["PK_COD_ACCESO"]; ?>">
                            <label class="text-success"><?php echo $value["NOM_ACCESO"]; ?></label>
                            <div class="row">
                            <?php
                            $roles = ctrRoles::consultarAcciones();
                            foreach ($roles as $clave => $valor) {
                            ?>
                                    <div class="col-1"></div>
                                    <div class="col-2">
                                        <input type="checkbox" class="campos" name="acciones<?php echo $value["PK_COD_ACCESO"]; ?>[]" value="<?php echo $valor["PK_COD_ACCION"]; ?>">
                                        <label class="text-dark"><?php echo $valor["NOM_ACCION"]; ?></label>
                                    </div>
                            <?php
                            }
                            ?>
                             </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar cambios">
                </div>
                <?php

                $agregar = new ctrRoles;
                $agregar->agregarNuevoRol();

                ?>
            </form>
        </div>

    </div>

</div>


<!-- Formulario de eliminar Rol -->
<div class="modal fade" id="eliminarRol">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Rol de Usuario</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>¿Esta seguro que desea eliminar el registro?</p>
                        <input type="hidden" id="codigoB" name="codigoB">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </div>
                <?php

                $eliminar = new ctrRoles;
                $eliminar->eliminarRol();

                ?>
            </form>
        </div>
    </div>
</div>

<script>
    $('document').ready(function() {
        $("#MarcarTodos").change(function() {
            $(".campos").prop('checked', $(this).prop("checked"));
        });
    });
</script>