<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>INICIAR UNA ORDEN DE TRABAJO</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Orden de trabajo</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <form method="post">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <div class="row">
                                    <label class="text-danger">* Código de cotización:</label><br>
                                    <div class="col-8">
                                        <div class="form-group">
                                            <input type="text" value="<?php
                                                                        if (isset($_SESSION["ORDEN_TRABAJO"]["idCotizacion"])) {
                                                                            echo $_SESSION["ORDEN_TRABAJO"]["idCotizacion"];
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#seleccionarCotizacion">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                    <label> Nombre del cliente:</label> <br>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" value="<?php
                                                                        if (isset($_SESSION["ORDEN_TRABAJO"]["nombreCliente"])) {
                                                                            echo $_SESSION["ORDEN_TRABAJO"]["nombreCliente"];
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <label class="text-danger">* Responsable del trabajo:</label><br>
                                    <div class="col-8">
                                        <div class="form-group">
                                            <input type="hidden" value="<?php
                                                                        if (isset($_SESSION["ORDEN_TRABAJO"]["codEmpleado"])) {
                                                                            echo $_SESSION["ORDEN_TRABAJO"]["codEmpleado"];
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>" class="form-control" disabled>
                                            <input type="text" class="form-control" value="<?php
                                                                                            if (isset($_SESSION["ORDEN_TRABAJO"]["nombreEmpleado"])) {
                                                                                                echo $_SESSION["ORDEN_TRABAJO"]["nombreEmpleado"];
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#seleccionarEmpleado">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                    <div class="col-12">
                                        <label class="text-danger">* Fecha de entrega:</label><br>
                                        <div class="form-group">
                                            <input type="date" min="<?php echo date("Y-m-d"); ?>" class="form-control" name="fechaEntrega">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="row">
                                    <div class="col-12">
                                        <label>Productos cotizados</label>
                                        <table class="table table-striped registros">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>Nombre</th>
                                                    <th>Descripcion</th>
                                                    <th style="width: 20px">Cantidad</th>
                                                    <th style="width: 20px">Precio</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (isset($_SESSION["ORDEN_TRABAJO"]["detalleCotizacion"])) {
                                                foreach ($_SESSION["ORDEN_TRABAJO"]["detalleCotizacion"] as $key => $value) {
                                                ?>
                                                    <tr>
                                                        <td><?php echo $value["COD_PRODUCTO"]; ?></td>
                                                        <td><?php echo $value["NOMBRE"]; ?></td>
                                                        <td><?php echo $value["DETALLE"]; ?></td>
                                                        <td><?php echo $value["CANT_PRODUCTO"]; ?></td>
                                                        <td><?php echo number_format($value["PRECIO_PRODUCTO"], 2); ?></td>
                                                        <td><?php echo number_format($value["TOTAL_PAGAR_PRODUCTO"], 2); ?></td>
                                                    </tr>
                                                <?php
                                                }}
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- div class="col-12">
                                        <div class="row">
                                            <div class="col-8">
                                                <h5>Lista de materiales de inventario para la orden de trabajo.</h5>
                                            </div>
                                            <div class="col-4">
                                                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#agregarMaterial">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div><br>
                                            <div class="col-12">
                                                <label>Materiales necesarios</label>
                                                <table class="table table-striped registros">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 10px">#</th>
                                                            <th>Nombre</th>
                                                            <th style="width: 20px">Cantidad</th>
                                                            <th style="width: 20px">Precio</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div -->
                                </div>
                            </div>
                            <div class="col-12">
                                <label>Observaciones:</label><br>
                                <div class="form-group">
                                    <textarea name="notas" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-3">
                                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#cancelarOrden">
                                    Cancelar Orden
                                </button>
                            </div>
                            <div class="col-4"></div>
                            <div class="col-5">
                                <input type="submit" value="Guardar Orden" class="btn btn-block btn-success">
                            </div>
                        </div>
                        <?php
                        
                        $crear = new ctrOrdenDeTrabajo;
                        $crear -> agregarOrden();


                        ?>
                </form>
            </div>
        </div>
    </section>
</div>


<div class="modal fade" id="seleccionarCotizacion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar Cotización</h4>
                </div>
                <div class="modal-body">
                    <table id="registros" style="width: 100%;" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>SubTotal</th>
                                <th>Descuento</th>
                                <th>ISV</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $respuesta = ctrCotizacion::consultarCotizacionesExistentes();
                            foreach ($respuesta as $key => $value) {

                            ?>
                                <tr>
                                    <td><?php echo $value["PK_COD_COTIZACION"]; ?></td>
                                    <td><?php echo $value["NOM_CLIENTE"] . " " . $value["APE_CLIENTE"]; ?></td>
                                    <td><?php echo date_format(date_create($value["FEC_COTIZACION"]), "d-m-Y"); ?></td>
                                    <td><?php echo date_format(date_create($value["HORA_COTIZACION"]), "h:i a"); ?></td>
                                    <td><?php echo number_format($value["SUBTOTAL"], 2); ?></td>
                                    <td><?php echo number_format($value["DESCUENTO"], 2); ?></td>
                                    <td><?php echo number_format($value["ISV"], 2); ?></td>
                                    <td><?php echo number_format($value["TOTAL_PAGAR"], 2); ?></td>
                                    <td style="width: 10%;">
                                        <form method="post">
                                            <input type="hidden" name="idCotizacion" value="<?php echo $value["PK_COD_COTIZACION"]; ?>">
                                            <input type="hidden" name="codCliente" value="<?php echo $value["PK_COD_CLIENTE"]; ?>">
                                            <input type="hidden" name="nombreCliente" value="<?php echo $value["NOM_CLIENTE"] . " " . $value["APE_CLIENTE"]; ?>">
                                            <button class="btn btn-success" type="submit">
                                                <i class="fas fa-check"></i>
                                            </button>
                                            <?php
                                            $seleccionar = new ctrOrdenDeTrabajo;
                                            $seleccionar->seleccionarCotizacion();
                                            ?>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal fade" id="seleccionarEmpleado">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar empleado responsable</h4>
                </div>
                <div class="modal-body">
                    <table id="registros" style="width: 100%;" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre Completo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $respuesta = ctrUsuarios::consultarEmpleados();
                            foreach ($respuesta as $key => $value) {

                            ?>
                                <tr>
                                    <td><?php echo $value["PK_COD_EMPLEADO"];  ?></td>
                                    <td><?php echo $value["NOMBRE"] . " " . $value["APELLIDO"]; ?></td>
                                    <td style="width: 10%;">
                                        <form method="post">
                                            <input type="hidden" name="codEmpleado" value="<?php echo $value["PK_COD_EMPLEADO"]; ?>">
                                            <input type="hidden" name="nombreEmpleado" value="<?php echo $value["NOMBRE"] . " " . $value["APELLIDO"]; ?>">
                                            <button class="btn btn-success" type="submit">
                                                <i class="fas fa-check"></i>
                                            </button>
                                            <?php
                                            $seleccionar = new ctrOrdenDeTrabajo;
                                            $seleccionar->seleccionarEmpleado();
                                            ?>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="agregarMaterial">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Selecciona el material</h4>
            </div>
            <div class="modal-body">
                <table id="registros" style="width: 100%;" class="table responsive table-bordered table-hover TB">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Detalle</th>
                            <th>Precio</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $respuesta = ctrOrdenDeTrabajo::consultarMateriales();                        
                        foreach ($respuesta as $key => $value) {

                        ?>
                            <tr>
                                <td style="width: auto;"><?php echo $value["PK_COD_MATERIAL"]; ?></td>
                                <td style="width: auto;"><?php echo $value["NOM_MATERIAL"]; ?></td>
                                <td style="width: auto;"><?php echo $value["DET_MATERIAL"];  ?></td>
                                <td style="width: auto;"><?php echo number_format($value["PRECIO_VENTA"], 2); ?></td>
                                <td style="width: 10%;">
                                    <center>
                                        <button cod="<?php echo $value["PK_COD_MATERIAL"]; ?>" type="button" class="btn btn-success agregarProductoExistente" data-toggle="modal" data-target="#agregarCantidad">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </center>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="agregarCantidad">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Cantidad de Material</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="hidden" name="codigoProducto" id="codigoProducto">

                    <div class="form-group">
                        <label for="cantidadMaterail">Cantidad de material:</label>
                        <input type="number" name="cantidadMaterail" id="cantidadMaterail" class="form-control">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Agregar">
                </div>

                <?php

                ?>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="cancelarOrden">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cancelar Orden de Trabajo</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>¿Seguro que deseas cancelar la orden de trabajo?</label>
                        <input type="hidden" name="Cancelar" value="1">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Cancelar orden">
                </div>

                <?php

                $cancelar = new ctrOrdenDeTrabajo();
                $cancelar->cancelarOrdenDeTrabajo();

                ?>
            </form>
        </div>
    </div>
</div>