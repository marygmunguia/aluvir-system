<?php
$permiso = ctrAccesosSistema::comprobarAcceso("inventario");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>REGISTRO DE CATEGORIAS</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Categorias</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevaCategoria">
                            Agregar Nueva Categoria
                        </button>
                    </div>
                    <div class="card-body">
                        <table id="registros" class="table responsive table-bordered table-hover TB">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Detalle</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $respuesta = ctrCategorias::consultarCategorias();
                                foreach ($respuesta as $key => $value) {

                                ?>
                                    <tr>
                                        <td><?php echo $value["PK_COD_CATEGORIA"]; ?></td>
                                        <td><?php echo $value["NOM_CATEGORIA"]; ?></td>
                                        <td><?php echo $value["DET_CATEGORIA"];  ?></td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_CATEGORIA"]; ?>" nombre="<?php echo $value["NOM_CATEGORIA"]; ?>" detalle="<?php echo $value["DET_CATEGORIA"]; ?>" type="button" class="btn btn-warning editarCategoria" data-toggle="modal" data-target="#actualizarCategoria">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </center>
                                        </td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_CATEGORIA"]; ?>" type="button" class="btn btn-danger eliminarCategoria" data-toggle="modal" data-target="#eliminarCategoria">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                        </table>
                    </div>

                </div>

            </div>

        </section>
    </div>

<?php
} else {
?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>


<!-- Formulario de Nueva Categoria -->
<div class="modal fade" id="nuevaCategoria">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Nueva Categoria</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nombre">Nombre de la categoria:</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="detalle">Detalle de categoria:</label>
                        <textarea name="detalle" class="form-control" id="detalle" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar cambios">
                </div>
                <?php

                $agregar = new ctrCategorias;
                $agregar->agregarNuevaCategoria();

                ?>
            </form>
        </div>

    </div>

</div>

<!-- Formulario de actualizar Categoria -->
<div class="modal fade" id="actualizarCategoria">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Actualizar Categoria</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="codigoE" name="codigoE">
                    <div class="form-group">
                        <label for="nombreE">Nombre de la categoria:</label>
                        <input type="text" class="form-control" name="nombreE" id="nombreE" required>
                    </div>
                    <div class="form-group">
                        <label for="detalleE">Detalle de categoria:</label>
                        <textarea name="detalleE" class="form-control" id="detalleE" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-warning" value="Aplicar cambios">
                </div>
                <?php

                $actualizar = new ctrCategorias;
                $actualizar->actualizarCategoria();

                ?>
            </form>
        </div>
    </div>
</div>

<!-- Formulario de eliminar Categoria -->
<div class="modal fade" id="eliminarCategoria">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Categoria</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>¿Esta seguro que desea eliminar el registro?</p>
                        <input type="hidden" id="codigoB" name="codigoB">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </div>
                <?php

                $eliminar = new ctrCategorias;
                $eliminar->eliminarCategoria();

                ?>
            </form>
        </div>
    </div>
</div>