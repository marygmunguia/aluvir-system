<?php
$permiso = ctrAccesosSistema::comprobarAcceso("compras");
if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>REGISTRAR INGRESO DE MATERIAL AL INVENTARIO</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Inventario</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        Actualizar existencias del material en inventario
                    </div>
                    <form action="" method="POST">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Seleccina el material:</label><br />
                                <select name="codMaterial" id="codMaterial" class="selectpicker form-control" data-live-search="true">
                                    <?php
                                    $respuesta = ctrRegistroInventario::ConsultarInventario();
                                    foreach ($respuesta as $key => $value) {
                                    ?>
                                        <option value="<?php echo $value["PK_COD_MATERIAL"]; ?>"><?php echo $value["NOM_MATERIAL"]; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Cantidad del material:</label><br />
                                <input type="number" name="cantidad" id="cantidad" class="form-control">
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-8"></div>
                                <div class="col-4">
                                <input type="submit" value="Registrar" class="btn btn-success btn-block">
                                </div>
                            </div>
                        </div>
                        <?php
                        $registrar = new ctrRegistroInventario();
                        $registrar -> registrarIngresoMaterial();
                        ?>
                    </form>
                </div>
            </div>
        </section>
    </div>


<?php
} else {
?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>