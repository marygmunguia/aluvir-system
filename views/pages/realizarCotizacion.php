<?php

ctrCotizacion::calcularCotizacion();

$permiso = ctrAccesosSistema::comprobarAcceso("cotizaciones");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Realizar nueva cotización</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Cotizaciones</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-7">
                                <h5>Realizar nueva cotización de producto para clientes</h5>
                            </div>
                            <div class="col-2">
                                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#cancelarCotizacion">
                                    Cancelar cotización
                                </button>
                            </div>
                            <div class="col-3">
                                <form method="post">
                                    <input type="submit" class="btn btn-primary btn-block" value="Terminar cotización">
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-5">
                                        <h5>Datos Generales</h5>
                                        <div class="form-group">
                                            <label>Nombre del cliente:</label>
                                            <div class="row">
                                                <div class="col-8">
                                                    <input type="hidden" name="idCliente" value="<?php
                                                                                                    if (isset($_SESSION["CLIENTE_COTIZACION"])) {
                                                                                                        echo $_SESSION["CLIENTE_COTIZACION"];
                                                                                                    } else {
                                                                                                        echo '';
                                                                                                    } ?>">
                                                    <input type="text" disabled class="form-control" value="<?php
                                                                                                            if (isset($_SESSION["NOM_CLIENTE_COTIZACION"])) {
                                                                                                                echo $_SESSION["NOM_CLIENTE_COTIZACION"];
                                                                                                            } else {
                                                                                                                echo '';
                                                                                                            }
                                                                                                            ?>">
                                                </div>
                                                <div class="col-4">
                                                    <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#seleccionarCliente">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php

                                        $guardar = new ctrCotizacion();
                                        $guardar->guardarCotizacion();

                                        ?>
                                        </form>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Usuario de sistema:</label>
                                                    <input type="text" value="<?php echo $_SESSION["NOMBRE_USUARIO"] ?>" disabled class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Fecha de cotización:</label>
                                                    <input type="text" value="<?php echo date('d/m/Y'); ?>" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <h5>Total de cotización</h5>
                                        <div class="form-group">
                                            <label>Subtotal:</label>
                                            <input type="text" disabled class="form-control" value="<?php
                                                                                                    if (isset($_SESSION["sumaSubtotal"])) {
                                                                                                        echo number_format($_SESSION["sumaSubtotal"], 2);
                                                                                                    } else {
                                                                                                        echo '0.00';
                                                                                                    }
                                                                                                    ?>">
                                        </div>
                                        <form method="post">
                                            <div class="row">
                                                <div class="col-6">
                                                    <label>Porcentaje de descuento (%):</label>
                                                    <input type="number" value="<?php
                                                                                if (isset($_SESSION["porcentaje_descuento"])) {
                                                                                    echo $_SESSION["porcentaje_descuento"];
                                                                                } else {
                                                                                    echo '0';
                                                                                }
                                                                                ?>" id="porcentaje_descuento" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico" min="0" max="100" class="form-control" name="porcentaje_descuento">
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <label>Descuento:</label>
                                                        <input type="text" value="<?php
                                                                                    if (isset($_SESSION["descuento"])) {
                                                                                        echo number_format($_SESSION["descuento"], 2);
                                                                                    } else {
                                                                                        echo '0.00';
                                                                                    }
                                                                                    ?>" disabled class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <label for="">Aplicar</label>
                                                    <center>
                                                        <button type="submit" class="btn btn-primary btn-block">
                                                            <i class="fas fa-check"></i>
                                                        </button>
                                                    </center>
                                                </div>
                                            </div>
                                            <?php

                                            $aplicar = new ctrCatalogo();
                                            $aplicar->calcularDescuento();

                                            ?>
                                        </form>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Impuesto sobre venta:</label>
                                                    <input type="text" value="<?php
                                                                                if (isset($_SESSION["isv"])) {
                                                                                    echo number_format($_SESSION["isv"], 2);
                                                                                } else {
                                                                                    echo '0.00';
                                                                                }
                                                                                ?>" disabled class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label>Total de cotización:</label>
                                                    <input type="text" value="<?php
                                                                                if (isset($_SESSION["sumaTotal"])) {
                                                                                    echo number_format($_SESSION["sumaTotal"], 2);
                                                                                } else {
                                                                                    echo '0.00';
                                                                                }
                                                                                ?>" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="card">
                                            <div class="card-header">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#nuevoProducto">
                                                    Agregar nuevo producto
                                                </button>
                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#agregarProducto">
                                                    Agregar producto existente
                                                </button>
                                            </div>
                                            <div class="card-body p-0">
                                                <table class="table table-striped registros">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 10px">#</th>
                                                            <th>Nombre</th>
                                                            <th>Descripcion</th>
                                                            <th style="width: 20px">Cantidad</th>
                                                            <th style="width: 20px">Precio</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if (isset($_SESSION["cotizacionProductos"])) {

                                                            foreach ($_SESSION["cotizacionProductos"] as $key => $value) {

                                                        ?>

                                                                <tr>
                                                                    <td><?php echo $value["PK_COD_PRODUCTO"]; ?></td>
                                                                    <td><?php echo $value["NOMBRE"]; ?></td>
                                                                    <td><?php echo $value["DETALLE"]; ?></td>
                                                                    <td><?php echo $value[0]; ?></td>
                                                                    <td><?php echo $value["PRECIO"]; ?></td>
                                                                    <td>
                                                                        <center>
                                                                            <button type="button" codigo="<?php echo $value["PK_COD_PRODUCTO"]; ?>" class="btn btn-danger eliminarProducto" data-toggle="modal" data-target="#eliminarProducto">
                                                                                <i class="fas fa-trash-alt"></i>
                                                                            </button>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php
} else {
?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>

<!-- Formulario de Nuevo Producto -->
<div class="modal fade" id="nuevoProducto">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Nuevo Producto</h4>
                </div>
                <div class="modal-body">
                    <h6>Datos de producto</h6>
                    <div class="form-group">
                        <label for="nombre">Nombre del producto:</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="detalle">Detalle de producto:</label>
                        <textarea name="detalle" class="form-control" id="detalle" rows="5"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="precio">Precio del producto: </label>
                                <input type="text" title="Debe de ingresar un valor númerico" class="form-control" pattern="\d+\.?\d*" name="precio" id="precio" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Imagen de producto: </label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Seleccionar imagen</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h6>Datos de cotización</h6>
                    <div class="form-group">
                        <label for="cantidad">Cantidad del producto en la cotización: </label>
                        <input type="number" class="form-control" name="cantidad" id="cantidad" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar">
                </div>
                <?php

                $guardar = new ctrCatalogo();
                $guardar->nuevoProducto();

                ?>
            </form>
        </div>

    </div>

</div>


<!-- Formulario de Producto Existente -->
<div class="modal fade" id="agregarProducto">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Selecciona un producto existente</h4>
            </div>
            <div class="modal-body">
                <table id="registros" style="width: 100%;" class="table responsive table-bordered table-hover TB">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Detalle</th>
                            <th>Precio</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $respuesta = ctrCatalogo::consultarProductosCatalogo();
                        foreach ($respuesta as $key => $value) {

                        ?>
                            <tr>
                                <td style="width: auto;"><?php echo $value["PK_COD_PRODUCTO"]; ?></td>
                                <td style="width: auto;"><?php echo $value["NOMBRE"]; ?></td>
                                <td style="width: auto;"><?php echo $value["DETALLE"];  ?></td>
                                <td style="width: auto;"><?php echo number_format($value["PRECIO"], 2); ?></td>
                                <td style="width: 10%;">
                                    <center>
                                        <button cod="<?php echo $value["PK_COD_PRODUCTO"]; ?>" type="button" class="btn btn-success agregarProductoExistente" data-toggle="modal" data-target="#agregarProductoSeleccionado">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </center>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Formulario de Producto Existente -->
<div class="modal fade" id="agregarProductoSeleccionado">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Producto Existente</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="hidden" name="codigoProducto" id="codigoProducto">

                    <div class="form-group">
                        <label for="cantidadProducto">Cantidad de producto:</label>
                        <input type="number" name="cantidadProducto" id="cantidadProducto" class="form-control">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Agregar">
                </div>

                <?php

                $agregarProductoExistente = new ctrCatalogo;
                $agregarProductoExistente->agregarProductoExistente();

                ?>
            </form>
        </div>
    </div>
</div>


<!-- Formulario de Eliminar Producto -->
<div class="modal fade" id="eliminarProducto">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Eliminar Producto</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="hidden" name="codigoProductoE" id="codigoProductoE">

                    <div class="form-group">
                        <label>¿Seguro que deseas eliminar el producto de la cotización?</label>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </div>

                <?php

                $eliminarProducto = new ctrCatalogo;
                $eliminarProducto->eliminarProducto();

                ?>
            </form>
        </div>
    </div>
</div>


<!-- Formulario de Cancelar Cotización -->
<div class="modal fade" id="cancelarCotizacion">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cancelar Cotización</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>¿Seguro que deseas cancelar la cotización?</label>
                        <input type="hidden" name="Cancelar" value="1">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Cancelar cotización">
                </div>

                <?php

                $cancelar = new ctrCotizacion();
                $cancelar->cancelarCotizacion();

                ?>
            </form>
        </div>
    </div>
</div>


<!-- Formulario de Seleccionar cliente -->
<div class="modal fade" id="seleccionarCliente">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Seleccionar Cliente</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nombre del cliente:</label><br/>
                        <select name="codCliente" id="codCliente" class="selectpicker form-control" data-live-search="true">
                            <?php
                            $respuesta = ctrClientes::consultarClientes();
                            foreach ($respuesta as $key => $value) {
                            ?>
                                <option value="<?php echo $value["PK_COD_CLIENTE"]; ?>"><?php echo $value["NOM_CLIENTE"]; ?> <?php echo $value["APE_CLIENTE"]; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Seleccionar cliente">
                </div>

                <?php

                $seleccionar = new ctrCotizacion();
                $seleccionar->seleccionarCliente();

                ?>
            </form>
        </div>
    </div>
</div>