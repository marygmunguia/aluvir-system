<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>VISUALIZAR VENTAS</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Ventas</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    Visualizar registro de ventas en el sistema
                </div>
                <div class="card-body">
                    <table id="registros" style="width: 100%;" class="table responsive table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Cliente</td>
                                <td>Encargado</td>
                                <td>Fecha</td>
                                <td>Hora</td>
                                <td>Tipo de Pago</td>
                                <td>Método de Pago</td>
                                <td>Subtotal</td>
                                <td>Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $respuesta = ctrVentas::consultarVentas();
                            foreach ($respuesta as $key => $value) {
                            ?>
                                <tr>
                                    <td><?php echo $value["PK_COD_VENTA"]; ?></td>
                                    <td><?php echo ucwords(strtolower($value["NOM_CLIENTE"])) . " " . ucwords(strtolower($value["APE_CLIENTE"])); ?></td>
                                    <td><?php echo ucwords(strtolower($value["NOMBRE"])) . " " . ucwords(strtolower($value["APELLIDO"])); ?></td>
                                    <td><?php echo date_format(date_create($value["FEC_VENTA"]), "d-m-Y"); ?></td>
                                    <td><?php echo date_format(date_create($value["HORA_VENTA"]), "h:i a"); ?></td>
                                    <td><?php echo ucwords(strtolower($value["TIPO_PAGO"])); ?></td>
                                    <td><?php echo ucwords(strtolower($value["NOM_METODO_PAGO"])); ?></td>
                                    <td><?php echo number_format($value["SUBTOTAL"], 2); ?></td>
                                    <td><?php echo number_format($value["TOTAL_PAGAR"], 2); ?></td>
                                    <td>
                                        <center>
                                            <button type="button" codigo="<?php echo $value["PK_COD_VENTA"]; ?>" class="btn btn-primary btnImprimirFactura"><i class="fas fa-print"></i>
                                            </button>
                                        </center>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $("#registros").on("click", ".btnImprimirFactura", function() {
        var codigo = $(this).attr("codigo");

        window.open(
            "tools/extension/tcpdf/examples/ventas/factura.php?codigo=" +
            codigo,
            "_black"
        );
    });
</script>