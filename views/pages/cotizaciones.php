<?php
$permiso = ctrAccesosSistema::comprobarAcceso("cotizaciones");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>REGISTRO DE COTIZACIONES</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Cotizaciones</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <table id="cotizaciones" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>SubTotal</th>
                                    <th>Descuento</th>
                                    <th>ISV</th>
                                    <th>Total</th>
                                    <th>Imprimir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $respuesta = ctrCotizacion::consultarCotizacionesExistentes();
                                foreach ($respuesta as $key => $value) {

                                ?>
                                    <tr>
                                        <td><?php echo $value["PK_COD_COTIZACION"]; ?></td>
                                        <td><?php echo $value["NOM_CLIENTE"] . " " . $value["APE_CLIENTE"]; ?></td>
                                        <td><?php echo date_format(date_create($value["FEC_COTIZACION"]), "d-m-Y"); ?></td>
                                        <td><?php echo date_format(date_create($value["HORA_COTIZACION"]), "h:i a"); ?></td>
                                        <td><?php echo number_format($value["SUBTOTAL"], 2); ?></td>
                                        <td><?php echo number_format($value["DESCUENTO"], 2); ?></td>
                                        <td><?php echo number_format($value["ISV"], 2); ?></td>
                                        <td><?php echo number_format($value["TOTAL_PAGAR"], 2); ?></td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button type="button" codigo="<?php echo $value["PK_COD_COTIZACION"]; ?>" class="btn btn-primary btnImprimirCotizacion"><i class="fas fa-print"></i>
                                                </button>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                        </table>
                    </div>

                </div>

            </div>

        </section>
    </div>

<?php
} else {
?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>

<script>
    $(document).ready(function() {
        $("#cotizaciones").DataTable({
            language: {
                sSearch: "Buscar:",
                sEmptyTable: "No hay datos disponibles",
                sZeroRecords: "No se han encontrado resultados",
                sInfo: "Mostrando registros del _START_ al _END_ de un total _TOTAL_",
                SInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0",
                sInfoFiltered: "(filtrando de un total de _MAX_ registrados)",
                oPaginate: {
                    sFirst: "Primero",
                    sLast: "Último",
                    sNext: "Siguiente",
                    sPrevious: "Anterior",
                },

                sLoadingRecords: "Cargando...",
                sLengthMenu: "Mostrar _MENU_ registros",
            },
            order: [
                [0, "desc"]
            ],
        });
    });
</script>