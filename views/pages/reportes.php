<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>REPORTES</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Reportes</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    Generar reportes
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 col-3">

                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>
                                        <center>Clientes</center>
                                    </h3>
                                </div>
                                <div class="small-box-footer">
                                    <button class="btn" onclick="reporteClientes();" style="color: #fff;">Generar <i class="fas fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-3">
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3>
                                        <center>Proveedores</center>
                                    </h3>
                                </div>
                                <div class="small-box-footer">
                                    <button class="btn" style="color: #fff;" onclick="reporteProveedores();">Generar <i class="fas fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-3">
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3>
                                        <center>Inventario</center>
                                    </h3>
                                </div>
                                <div class="small-box-footer">
                                    <button class="btn" style="color: #fff;" onclick="reporteInventario();">Generar <i class="fas fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-3">
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3>
                                        <center>Usuarios</center>
                                    </h3>
                                </div>
                                <div class="small-box-footer">
                                    <button class="btn" style="color: #fff;" onclick="reporteUsuarios();">Generar <i class="fas fa-arrow-circle-right"></i></button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <form method="post">
                    <div class="card card-danger">
                        <div class="card-header">
                            Generar reporte de ventas por fecha
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="text-danger">* Fecha Inicio:</label><br>
                                        <input type="date" max="<?php echo date("Y-m-d"); ?>" required class="form-control" name="fechaInicio">
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <label class="text-danger">* Fecha Final:</label><br>
                                        <input type="date" class="form-control" name="fechaFinal" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-8"></div>
                                <div class="col-4">
                                    <input type="submit" value="Generar" class="btn btn-block btn-danger">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php

                    $generar = new ctrVentas;
                    $generar->generarReportePorFechas();

                    ?>
                </form>
            </div>

            <section class="content">
                <div class="container-fluid">
                    <br>
                    <h2>Reportes Grafícos</h2><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Productos más vendidos</h3>
                                </div>
                                <div class="card-body">
                                    <div class="chart">
                                        <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card card-danger">
                            <div class="card-header">
                                <h3 class="card-title">Fechas de mayores ventas</h3>
                            </div>
                            <div class="card-body">
                                <div class="chart">
                                    <canvas id="areaChart1" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>


<script>
    function reporteInventario() {
        window.open(
            "tools/extension/tcpdf/examples/reportes/reporteInventario.php",
            "_black"
        );
    }


    $(function() {

        var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

        var areaChartData = {
            labels: [<?php
                        $res = ctrVentas::productosMasVendidos();
                        foreach ($res as $key => $value) {
                        ?> '<?php echo $value["NOMBRE"]; ?>',
                <?php
                        }
                ?>
            ],
            datasets: [{
                backgroundColor: 'rgba(60,141,188,0.9)',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: [
                    <?php
                    $res = ctrVentas::productosMasVendidos();
                    foreach ($res as $key => $value) {
                    ?> '<?php echo $value["SUM(dc.CANT_PRODUCTO)"]; ?>',
                    <?php
                    }
                    ?>
                ]
            }, ]
        }

        var areaChartOptions = {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false,
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: false,
                    }
                }]
            }
        }

        new Chart(areaChartCanvas, {
            type: 'line',
            data: areaChartData,
            options: areaChartOptions
        })

        var areaChart1Canvas = $('#areaChart1').get(0).getContext('2d')

        var areaChart1Data = {
            labels: [<?php
                        $res = ctrVentas::fechaMasVentas();
                        foreach ($res as $key => $value) {
                        ?> '<?php echo date_format(date_create($value["FEC_VENTA"]), "d-m-Y"); ?>',
                <?php
                        }
                ?>
            ],
            datasets: [{
                backgroundColor: 'rgba(200, 10, 10, 1)',
                borderColor: 'rgba(210, 214, 222, 1)',
                pointRadius: false,
                pointColor: 'rgba(210, 214, 222, 1)',
                pointStrokeColor: '#c1c7d1',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: [
                    <?php
                    $res = ctrVentas::fechaMasVentas();
                    foreach ($res as $key => $value) {
                    ?> '<?php echo $value["SUM(v.TOTAL_PAGAR)"]; ?>',
                    <?php
                    }
                    ?>
                ]
            }, ]
        }

        var areaChart1Options = {
            maintainAspectRatio: false,
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false,
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: false,
                    }
                }]
            }
        }

        new Chart(areaChart1Canvas, {
            type: 'line',
            data: areaChart1Data,
            options: areaChart1Options
        })
    })
</script>