<?php
$permiso = ctrAccesosSistema::comprobarAcceso("clientes");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>REGISTRO DE METODOS DE PAGO</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Metodos de Pago</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#agregarMetodopago">
                            Agregar Metodo de Pago
                        </button>
                    </div>
                    <div class="card-body">
                        <table id="registros" class="table table-bordered table-hover TB">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Metodo de Pago</th>
                                    <th>Detalle del Metodo de Pago</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $respuesta = ctrMetodospago::consultarMetodospago();
                                foreach ($respuesta as $key => $value) {

                                ?>
                                    <tr>
                                        <td><?php echo $value["PK_COD_METODO_PAGO"]; ?></td>
                                        <td><?php echo $value["NOM_METODO_PAGO"]; ?></td>
                                        <td><?php echo $value["DET_METODO_PAGO"]; ?></td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_METODO_PAGO"]; ?>" nombre="<?php echo $value["NOM_METODO_PAGO"]; ?>" detalle="<?php echo $value["DET_METODO_PAGO"]; ?>" type="button" class="btn btn-warning editarMetodopago" data-toggle="modal" data-target="#actualizarMetodopago">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </center>
                                        </td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_METODO_PAGO"]; ?>" type="button" class="btn btn-danger eliminarMetodopago" data-toggle="modal" data-target="#eliminarMetodopago">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                        </table>
                    </div>

                </div>

            </div>

        </section>
    </div>

<?php
} else {
?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del
                        sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>


<!-- Formulario de Nuevo Metodo de pago -->
<div class="modal fade" id="agregarMetodopago">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Metodo de Pago</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nombre">Nombre del metodo de pago:</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="detalle">Detalle del metodo de pago:</label>
                        <textarea name="detalle" class="form-control" id="detalle" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar cambios">
                </div>
                <?php

                $agregar = new ctrMetodospago;
                $agregar->agregarMetodopago();

                ?>
            </form>
        </div>

    </div>

</div>

<!-- Formulario de eliminar el Metodo de Pago -->
<div class="modal fade" id="eliminarMetodopago">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Metodo de Pago</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>¿Esta seguro que desea eliminar el registro?</p>
                        <input type="hidden" id="codigoB" name="codigoB">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </div>
                <?php

                $eliminar = new ctrMetodospago;
                $eliminar->eliminarMetodopago();

                ?>
            </form>
        </div>
    </div>
</div>


<!-- Formulario de actualizar Metodo de Pago -->
<div class="modal fade" id="actualizarMetodopago">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Actualizar Metodo de Pago</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="codigoE" name="codigoE">
                    <div class="form-group">
                        <label for="nombreE">Nombre del Metodo de Pago:</label>
                        <input type="text" class="form-control" name="nombreE" id="nombreE" required>
                    </div>
                    <div class="form-group">
                        <label for="detalleE">Detalle del Metodo de Pago:</label>
                        <textarea name="detalleE" class="form-control" id="detalleE" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-warning" value="Aplicar cambios">
                </div>
                <?php

                $actualizar = new ctrMetodospago();
                $actualizar->actualizarMetodopago();


                ?>
            </form>
        </div>
    </div>
</div>