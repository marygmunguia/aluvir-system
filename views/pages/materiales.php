<?php
$permiso = ctrAccesosSistema::comprobarAcceso("materiales");

if ($permiso === false) {
?>
    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}

?>

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>DETALLE DE MATERIALES</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item active">Materiales</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Programacion de el data table  -->
    <section class="content">
        <div class="container-fluid">

            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevoMaterial">
                        Registrar Nuevo Material
                    </button>
                </div>
                <div class="card-body">
                    <table id="registros" class="table table-bordered table-hover TB">
                        <thead>
                            <tr>
                                <th>Id. Material</th>
                                <th>Cod. Bara</th>
                                <th>Nombre Material</th>
                                <th>Descripcion</th>
                                <th>Categoria</th>
                                <th>Precio a Facturar</th>
                                <th>Existencias</th>
                                <th>Existencias Minimas</th>
                                <th>Existencias Maximas</th>
                                <th>EDITAR</th>
                                <th>OCULTAR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $respuesta = ctrMateriales::consultarMateriales();
                            foreach ($respuesta as $key => $value) {

                            ?>
                                <tr>
                                    <td><?php echo $value["PK_COD_MATERIAL"]; ?></td>
                                    <td><?php echo $value["COD_BARRAS"]; ?></td>
                                    <td><?php echo $value["NOM_MATERIAL"];  ?></td>
                                    <td><?php echo $value["DET_MATERIAL"]; ?></td>

                                    <td><?php

                                        $categoria = ctrCategorias::consultarCategoria($value["COD_CATEGORIA"]);

                                        echo $categoria["NOM_CATEGORIA"];

                                        ?></td>
                                    <td><?php echo $value["PRECIO_VENTA"]; ?></td>
                                    <td><?php echo $value["EXISTENCIAS"];  ?></td>
                                    <td><?php echo $value["MIN_EXISTENCIAS"]; ?></td>
                                    <td><?php echo $value["MAX_EXISTENCIAS"]; ?></td>
                                    <td>





                                        <center>
                                            <button cod="<?php echo $value["PK_COD_MATERIAL"]; ?>" codBarra="<?php echo $value["COD_BARRAS"]; ?>" nombre="<?php echo $value["NOM_MATERIAL"]; ?>" detalles="<?php echo $value["DET_MATERIAL"]; ?>" codCategoria="<?php echo $value["COD_CATEGORIA"]; ?>" precioVenta="<?php echo $value["PRECIO_VENTA"]; ?>" minexis="<?php echo $value["MIN_EXISTENCIAS"]; ?>" maxexis="<?php echo $value["MAX_EXISTENCIAS"]; ?>" type="button" class="btn btn-warning editarMaterial" data-toggle="modal" data-target="#actualizarMaterial">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                        </center>
                                    </td>
                                    <td style="width: 10%;">
                                        <center>
                                            <button cod="<?php echo $value["PK_COD_MATERIAL"]; ?>" type="button" class="btn btn-danger eliminarMaterial" data-toggle="modal" data-target="#eliminarMaterial">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </center>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                    </table>
                </div>

            </div>

        </div>

    </section>
</div>

<!-- Formulario de Nuevo Material  -->

<div class="modal fade" id="nuevoMaterial">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nuevo Material</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="Cod_Barra">Codigo de Barra:</label>
                            <input type="text" class="form-control" name="Cod_Barra" id="Cod_Barra" pattern="^[0-9-]+$" title="Debe de ingresar un código válido">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="nombre">Nombre Material:</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="Detalle">Detalle de Material:</label>
                            <textarea class="form-control" name="Detalle" id="Detalle" rows="4" required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido"></textarea>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="tipo_categoria">Categoria de Material:</label>
                            <select name="tipo_categoria" id="tipo_categoria" class="form-control">
                                <?php
                                $opciones_tipo_identificacion = ctrCategorias::consultarCategorias();
                                var_dump($opciones_tipo_identificacion);
                                foreach ($opciones_tipo_identificacion as $key => $value) {
                                ?>
                                    <option value="<?php echo $value["PK_COD_CATEGORIA"]; ?>">
                                        <?php echo $value["NOM_CATEGORIA"]; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="precioventa">Precio de Venta:</label>
                            <input type="text" class="form-control" required maxlength="7" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico"  name="precioventa" id="precioventa" value="0.00">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="exismin">Minimas Existencias:</label>
                            <input type="text" class="form-control" required value="0" maxlength="7" name="exismin" id="exismin" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico" >
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="exismax">Maxima Existencias:</label>
                            <input type="text" class="form-control" required value="0" maxlength="7" name="exismax" id="exismax" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico" >
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Crear Material">
                </div>
                <?php

                $agregar = new ctrMateriales;
                $agregar->agregarMateriales();

                ?>
            </form>
        </div>

    </div>

</div>




<!-- Formulario de Actualizar Material  -->

<div class="modal fade" id="actualizarMaterial">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Actualizar Registro de Material</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <input type="hidden" id="codigoE" name="codigoE">
                        <div class="form-group">
                            <label for="codBarraE">Codigo de Barra:</label>
                            <input type="text" class="form-control" name="codBarraE" id="codBarraE">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="nombreE">Nombre de Material:</label>
                            <input type="text" class="form-control" name="nombreE" id="nombreE" required>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="detallesE">Detalle de Material:</label>
                            <textarea class="form-control" name="detallesE" id="detallesE" required rows="4"></textarea>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="CodCategoriaE">Categoria de Material:</label>
                            <select name="CodCategoriaE" id="CodCategoriaE" class="form-control">
                                <?php
                                $opciones_tipo_identificacion = ctrCategorias::consultarCategorias();
                                foreach ($opciones_tipo_identificacion as $key => $value) {
                                ?>
                                    <option value="<?php echo $value["PK_COD_CATEGORIA"]; ?>">
                                        <?php echo $value["NOM_CATEGORIA"]; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="precioVentaE">Precio de Venta:</label>
                            <input type="text" class="form-control" name="precioVentaE" id="precioVentaE" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico"  value="0.00">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="minexisE">Existencia Minima:</label>
                            <input type="text" class="form-control" name="minexisE" id="minexisE">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="maxexisE">Existencia Maxima:</label>
                            <input type="text" class="form-control" name="maxexisE" id="maxexisE">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar cambios">
                </div>
                <?php

                $actualizar = new ctrMateriales;
                $actualizar->actualizarMateriales();

                ?>
            </form>
        </div>

    </div>

</div>

<!-- Formulario de eliminar/Ocultar Material -->
<div class="modal fade" id="eliminarMaterial">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Ocultar Registro de Material</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>¿Esta seguro que desea Ocultar el Material seleccionado?</p>
                        <input type="hidden" id="codigoB" name="codigoB">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Ocultar">
                </div>
                <?php

                $eliminar = new ctrMateriales;
                $eliminar->eliminarMateriales();

                ?>
            </form>
        </div>
    </div>
</div>