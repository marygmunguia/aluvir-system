<?php
$permiso = ctrAccesosSistema::comprobarAcceso("compras");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>REGISTRAR COMPRA DE MATERIALES</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Compras</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#registrarCompra">
                            Registrar Compra de Material
                        </button>
                    </div>
                    <div class="card-body">
                        <table id="registros" style="width: 100%;" class="table responsive table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th># Compra</th>
                                    <th># Factura</th>
                                    <th>Proveedor</th>
                                    <th>Fecha</th>
                                    <th>Subtotal</th>
                                    <th>Descuento</th>
                                    <th>ISV</th>
                                    <th>Total</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $respuesta = ctrCompras::consultarCompras();
                                foreach ($respuesta as $key => $value) {
                                ?>
                                    <tr>
                                        <td><?php echo $value["PK_COD_COMPRA"];  ?></td>
                                        <td><?php echo $value["NO_FACTURA_COMPRA"]; ?></td>
                                        <td><?php

                                            $proveedor = ctrCompras::consultarProveedor($value["COD_PROVEEDOR"]);
                                            echo $proveedor;

                                            ?></td>
                                        <td><?php echo date_format(date_create($value["FEC_COMPRA"]), "d-m-Y"); ?></td>
                                        <td><?php echo number_format($value["SUBTOTAL"], 2);  ?></td>
                                        <td><?php echo number_format($value["DESCUENTO"], 2); ?></td>
                                        <td><?php echo number_format($value["ISV"], 2);  ?></td>
                                        <td><?php echo number_format($value["TOTAL_PAGAR"], 2); ?></td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_COMPRA"]; ?>" 
                                                factura="<?php echo $value["NO_FACTURA_COMPRA"]; ?>" 
                                                proveedor="<?php echo $value["COD_PROVEEDOR"] ?>" 
                                                fecha="<?php echo date_format(date_create($value["FEC_COMPRA"]), "Y-m-d"); ?>" 
                                                subtotal="<?php echo number_format($value["SUBTOTAL"], 2); ?>"
                                                descuento="<?php echo number_format($value["DESCUENTO"], 2); ?>"
                                                isv="<?php echo number_format($value["ISV"], 2); ?>"
                                                total="<?php echo number_format($value["TOTAL_PAGAR"], 2); ?>"
                                                type="button" class="btn btn-warning editarCompra" data-toggle="modal" data-target="#editarCompra">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </center>
                                        </td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_COMPRA"]; ?>" type="button" class="btn btn-danger eliminarCompra" data-toggle="modal" data-target="#eliminarCompra">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php
} else {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>

<div class="modal fade" id="eliminarCompra">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Registro de Compra</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>¿Esta seguro que desea eliminar el registro?</p>
                        <input type="hidden" id="codigoCompra" name="codigoCompra">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </div>
                <?php

                $eliminar = new ctrCompras;
                $eliminar->eliminarCompra();

                ?>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="registrarCompra">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registrar Nueva Compra</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <p class="text-danger">Todos los datos ingresado deben se iguales a la factura asociada.</p>
                    <div class="form-group">
                        <label> Número de factura:</label>
                        <input type="text" class="form-control" name="numeroFactura" id="numeroFactura" pattern="^[0-9-]+$" title="Debe de ingresar un número de factura válido" placeholder="000-000-00-00000" maxlength="25">
                    </div>
                    <div class="form-group">
                        <label> Selecciona a un proveedor:</label>
                        <select name="codProveedor" id="codProveedor" required class="selectpicker form-control" data-live-search="true">
                            <?php
                            $respuesta = ctrCompras::consultarProveedores();
                            foreach ($respuesta as $key => $value) {
                            ?>
                                <option value="<?php echo $value["PK_COD_PROVEEDOR"]; ?>"><?php echo $value["NOM_PROVEEDOR"]; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label> Fecha de la compra:</label>
                        <input type="date" class="form-control" required name="fechaCompra" id="fechaCompra" max="<?php echo date('Y-m-d'); ?>">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label> SubTotal:</label>
                                <input type="text" maxlength="7" required class="form-control" name="subtotal" id="subtotal" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label> Descuento:</label>
                                <input type="text" maxlength="7" class="form-control" required value="0.00" name="descuento" id="descuento" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label> Impuestos sobre venta:</label>
                                <input type="text" maxlength="7" required class="form-control" name="isv" id="isv" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label> Total pagado:</label>
                                <input type="text" maxlength="7" required class="form-control" name="total" id="total" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Registrar compra">
                </div>
                <?php

                $agregar = new ctrCompras();
                $agregar->agregarCompra();

                ?>
            </form>
        </div>

    </div>

</div>


<div class="modal fade" id="editarCompra">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registrar Nueva Compra</h4>
            </div>
            <form method="post">
                <div class="modal-body">
                    <p class="text-danger">Todos los datos ingresado deben se iguales a la factura asociada.</p>
                    <input type="hidden" id="codigoCompraE" name="codigoCompraE">
                    <div class="form-group">
                        <label> Número de factura:</label>
                        <input type="text" class="form-control" name="numeroFacturaE" id="numeroFacturaE" pattern="^[0-9-]+$" title="Debe de ingresar un número de factura válido" placeholder="000-000-00-00000" maxlength="25">
                    </div>
                    <div class="form-group">
                        <label> Selecciona a un proveedor:</label>
                        <select name="codProveedorE" id="codProveedorE" required class="form-control" data-live-search="true">
                            <?php
                            $respuesta = ctrCompras::consultarProveedores();
                            foreach ($respuesta as $key => $value) {
                            ?>
                                <option value="<?php echo $value["PK_COD_PROVEEDOR"]; ?>"><?php echo $value["NOM_PROVEEDOR"]; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label> Fecha de la compra:</label>
                        <input type="date" class="form-control" required name="fechaCompraE" id="fechaCompraE" max="<?php echo date('Y-m-d'); ?>">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label> SubTotal:</label>
                                <input type="text" maxlength="7" required class="form-control" name="subtotalE" id="subtotalE" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label> Descuento:</label>
                                <input type="text" maxlength="7" class="form-control" required value="0.00" name="descuentoE" id="descuentoE" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label> Impuestos sobre venta:</label>
                                <input type="text" maxlength="7" required class="form-control" name="isvE" id="isvE" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label> Total pagado:</label>
                                <input type="text" maxlength="7" required class="form-control" name="totalE" id="totalE" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Registrar compra">
                </div>
                <?php

                $agregar = new ctrCompras();
                $agregar->actualizarCompra();

                ?>
            </form>
        </div>

    </div>

</div>