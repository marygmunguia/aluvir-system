<?php
$permiso = ctrAccesosSistema::comprobarAcceso("clientes");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>REGISTRO DE DIRECCIONES</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">DIRECCIONES</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#agregarNuevaDireccion">
                            Agregar Nueva Direccion
                        </button>
                    </div>
                    <div class="card-body">
                        <table id="registros" class="table table-bordered table-hover TB">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Código Cliente</th>
                                    <th>Departamento</th>
                                    <th>Municipio</th>
                                    <th>Ciudad</th>
                                    <th>Colonia</th>
                                    <th>Detalle</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $respuesta = ctrDirecciones::consultarDirecciones();
                                foreach ($respuesta as $key => $value) {

                                ?>
                                    <tr>
                                        <td><?php echo $value["PK_COD_DIRECCION"]; ?></td>
                                        <td><?php echo $value["COD_CLIENTE"]; ?></td>
                                        <td><?php echo $value["DEPARTAMENTO"]; ?></td>
                                        <td><?php echo $value["MUNICIPIO"]; ?></td>
                                        <td><?php echo $value["CIUDAD"]; ?></td>
                                        <td><?php echo $value["BARRIO_COLONIA"]; ?></td>
                                        <td><?php echo $value["DET_DIRECCION"];  ?></td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_DIRECCION"]; ?>" cod_cliente="<?php echo $value["COD_CLIENTE"]; ?>" departamento="<?php echo $value["DEPARTAMENTO"]; ?>" municipio="<?php echo $value["MUNICIPIO"]; ?>" ciudad="<?php echo $value["CIUDAD"]; ?>" colonia="<?php echo $value["BARRIO_COLONIA"]; ?>" detalle="<?php echo $value["DET_DIRECCION"]; ?>" type="button" class="btn btn-warning editarDireccion" data-toggle="modal" data-target="#actualizarDireccion">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </center>
                                        </td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_DIRECCION"]; ?>" type="button" class="btn btn-danger eliminarDireccion" data-toggle="modal" data-target="#eliminarDireccion">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                        </table>
                    </div>

                </div>

            </div>

        </section>
    </div>

<?php
} else {
?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>


<!-- Formulario de Nueva Direccion -->
<div class="modal fade" id="agregarNuevaDireccion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Nueva Direccion</h4>
                </div>
                 <div class="modal-body">
                     <div class="form-group">
                        <label for="cod_cliente">Seleccionar al cliente:</label>
                        <select name="cod_cliente" id="cod_cliente" required class="selectpicker form-control" data-live-search="true">
                            <?php
                            $respuesta = ctrClientes::consultarClientes();
                            foreach ($respuesta as $key => $value) {
                            ?>
                                <option value="<?php echo $value["PK_COD_CLIENTE"]; ?>"><?php echo $value["NOM_CLIENTE"]; ?> <?php echo $value["APE_CLIENTE"]; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div> 
                    <div class="form-group">
                        <label for="departamento">Nombre del departamento:</label>
                        <input type="text" class="form-control" name="departamento" id="departamento" required>
                    </div>
                    <div class="form-group">
                        <label for="municipio">Nombre del municipio:</label>
                        <input type="text" class="form-control" name="municipio" id="municipio">
                    </div>
                    <div class="form-group">
                        <label for="ciudad">Nombre de la ciudad:</label>
                        <input type="text" class="form-control" name="ciudad" id="ciudad">
                    </div>
                    <div class="form-group">
                        <label for="colonia">Nombre de la colonia o barrio:</label>
                        <input type="text" class="form-control" name="colonia" id="colonia" required>
                    </div>
                    <div class="form-group">
                        <label for="detalle">Detalle de la direccion:</label>
                        <textarea name="detalle" class="form-control" id="detalle" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar cambios">
                </div>
                <?php

                $agregar = new ctrDirecciones;
                $agregar->agregarNuevaDireccion();

                ?>
            </form>
        </div>

    </div>

</div>

<!-- Formulario de actualizar Direccion -->
<div class="modal fade" id="actualizarDireccion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Actualizar Direccion</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="codigoE" name="codigoE">
                    <div class="form-group">
                        <label for="cod_clienteE">Codigo del cliente:</label>
                        <input type="text" class="form-control" name="cod_clienteE" id="cod_clienteE" required>
                    </div>
                    <div class="form-group">
                        <label for="departamentoE">Nombre del departamento:</label>
                        <input type="text" class="form-control" name="departamentoE" id="departamentoE" required>
                    </div>
                    <div class="form-group">
                        <label for="municipioE">Nombre del municipio:</label>
                        <input type="text" class="form-control" name="municipioE" id="municipioE">
                    </div>
                    <div class="form-group">
                        <label for="ciudadE">Nombre de la ciudad:</label>
                        <input type="text" class="form-control" name="ciudadE" id="ciudadE">
                    </div>
                    <div class="form-group">
                        <label for="coloniaE">Nombre de la colonia o barrio:</label>
                        <input type="text" class="form-control" name="coloniaE" id="coloniaE" required>
                    </div>
                    <div class="form-group">
                        <label for="detalleE">Detalle de la direccion:</label>
                        <textarea name="detalleE" class="form-control" id="detalleE" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-warning" value="Aplicar cambios">
                </div>
                <?php

                $actualizar = new ctrDirecciones;
                $actualizar->actualizarDireccion();

                ?>
            </form>
        </div>
    </div>
</div>

<!-- Formulario de eliminar Direccion -->
<div class="modal fade" id="eliminarDireccion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Direccion</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>¿Esta seguro que desea eliminar el registro?</p>
                        <input type="hidden" id="codigoB" name="codigoB">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </div>
                <?php

                $eliminar = new ctrDirecciones;
                $eliminar->eliminarDireccion();

                ?>
            </form>
        </div>
    </div>
</div>