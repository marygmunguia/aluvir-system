<?php
$permiso = ctrAccesosSistema::comprobarAcceso("clientes");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>GESTIÓN DE CLIENTES</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item active">Clientes</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">

                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevoCliente">
                            Registrar Nuevo Cliente
                        </button>
                    </div>
                    <div class="card-body">
                        <table id="registros" style="width: 100%;" class="table responsive table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Tipo de identificación</th>
                                    <th>No. de identificación</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Correo Electrónico</th>
                                    <th>Teléfono</th>
                                    <th>Celular</th>
                                    <th>Crédito</th>
                                    <th>Limite de crédito</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $respuesta = ctrClientes::consultarClientes();
                                foreach ($respuesta as $key => $value) {

                                ?>
                                    <tr>
                                        <td><?php echo $value["PK_COD_CLIENTE"]; ?></td>
                                        <td><?php

                                            $tipoIdentificacion = ctrIdentificaciones::consultarTipoIdentificacion($value["COD_TIPO_IDENTIFICACION"]);
                                            echo $tipoIdentificacion["NOM_TIPO_IDENTIFICACION"];

                                            ?></td>
                                        <td><?php echo $value["NO_IDENTIFICACION"];  ?></td>
                                        <td><?php echo $value["NOM_CLIENTE"]; ?></td>
                                        <td><?php echo $value["APE_CLIENTE"]; ?></td>
                                        <td><?php echo $value["EMAIL"];  ?></td>
                                        <td><?php echo $value["TELEFONO"]; ?></td>
                                        <td><?php echo $value["CELULAR"]; ?></td>
                                        <td><?php

                                            if ($value["CREDITO"] == 1) {
                                                echo 'SI';
                                            } else {
                                                echo 'NO';
                                            }

                                            ?></td>
                                        <td><?php echo $value["LIM_CREDITO"]; ?> Lps.</td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button onclick="activarLimiteCredito();" cod="<?php echo $value["PK_COD_CLIENTE"]; ?>" tipoIdentificacion="<?php echo $value["COD_TIPO_IDENTIFICACION"]; ?>" noIdentificacion="<?php echo $value["NO_IDENTIFICACION"]; ?>" nombre="<?php echo $value["NOM_CLIENTE"]; ?>" apellido="<?php echo $value["APE_CLIENTE"]; ?>" email="<?php echo $value["EMAIL"]; ?>" telefono="<?php echo $value["TELEFONO"]; ?>" celular="<?php echo $value["CELULAR"]; ?>" credito="<?php echo $value["CREDITO"]; ?>" limiteCredito="<?php echo $value["LIM_CREDITO"]; ?>" type="button" class="btn btn-warning editarCliente" data-toggle="modal" data-target="#actualizarCliente">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                            </center>
                                        </td>
                                        <td style="width: 10%;">
                                            <center>
                                                <button cod="<?php echo $value["PK_COD_CLIENTE"]; ?>" type="button" class="btn btn-danger eliminarCliente" data-toggle="modal" data-target="#eliminarCliente">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                        </table>
                    </div>

                </div>

            </div>
        </section>
    </div>
<?php
} else {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
?>


<!-- Formulario de Nuevo Cliente  -->

<div class="modal fade" id="nuevoCliente">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Registrar Nuevo Cliente</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="tipo_identificacion">Tipo de identificación:</label>
                            <select name="tipo_identificacion" id="tipo_identificacion" class="form-control">
                                <?php
                                $opciones_tipo_identificacion = ctrIdentificaciones::consultarTiposIdentificaciones();
                                foreach ($opciones_tipo_identificacion as $key => $value) {
                                ?>
                                    <option value="<?php echo $value["PK_COD_TIPO_IDENTIFICACION"]; ?>"><?php echo $value["NOM_TIPO_IDENTIFICACION"]; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="no_identificacion">Número de identificación:</label>
                            <input type="text" class="form-control" name="no_identificacion" id="no_identificacion" pattern="^[0-9-]+$" title="Debe de ingresar un número de identificación válido" placeholder="0000-0000-00000">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="nombre" class="text-danger">* Nombre:</label>
                            <input type="text" class="form-control" name="nombre" id="nombre" required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="apellido" class="text-danger">* Apellido:</label>
                            <input type="text" class="form-control" name="apellido" id="apellido" required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un apellido válido">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="correo">Correo Electrónico:</label>
                            <input type="email" class="form-control" name="correo" id="correo">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="telefono">Teléfono:</label>
                            <input type="text" class="form-control" name="telefono" id="telefono" placeholder="0000-0000" pattern="^[0-9-]+$" title="Debe de ingresar un número de teléfono válido">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="celular">Celular:</label>
                            <input type="text" class="form-control" name="celular" id="celular" placeholder="0000-0000" pattern="^[0-9-]+$" title="Debe de ingresar un número de celular válido">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="celular">Disponibilidad de crédito:</label>
                            <select name="credito" id="credito" class="form-control">
                                <option value="0">NO</option>
                                <option value="1">SI</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="limite_credito">Limite de crédito:</label>
                            <input type="text" class="form-control" disabled name="limite_credito" id="limite_credito" pattern="\d+\.?\d*" title="Debe de ingresar un valor númerico" value="0.00">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar cambios">
                </div>
                <?php

                $agregar = new ctrClientes;
                $agregar->agregarNuevoCliente();

                ?>
            </form>
        </div>

    </div>

</div>



<!-- Formulario de Actualizar Cliente  -->

<div class="modal fade" id="actualizarCliente">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Actualizar Registro de Cliente</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-6">
                        <div class="form-group">
                            <input type="hidden" id="codigoE" name="codigoE">
                            <label for="tipo_identificacionE">Tipo de identificación:</label>
                            <select name="tipo_identificacionE" id="tipo_identificacionE" class="form-control">
                                <?php
                                $opciones_tipo_identificacion = ctrIdentificaciones::consultarTiposIdentificaciones();
                                foreach ($opciones_tipo_identificacion as $key => $value) {
                                ?>
                                    <option value="<?php echo $value["PK_COD_TIPO_IDENTIFICACION"]; ?>"><?php echo $value["NOM_TIPO_IDENTIFICACION"]; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="no_identificacionE">Número de identificación:</label>
                            <input type="text" class="form-control" name="no_identificacionE" id="no_identificacionE" pattern="^[0-9-]+$" title="Debe de ingresar un número de identificación válido" placeholder="0000-0000-00000">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="nombreE">Nombre:</label>
                            <input type="text" class="form-control" name="nombreE" id="nombreE" required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un nombre válido">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="apellidoE">Apellido:</label>
                            <input type="text" class="form-control" name="apellidoE" id="apellidoE" required pattern="^[\p{L} \.'\-]+$" title="Debe de ingresar un apellido válido">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="correoE">Correo Electrónico:</label>
                            <input type="email" class="form-control" name="correoE" id="correoE">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="telefonoE">Teléfono:</label>
                            <input type="text" class="form-control" name="telefonoE" id="telefonoE" placeholder="0000-0000" pattern="^[0-9-]+$" title="Debe de ingresar un número de teléfono válido">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="celularE">Celular:</label>
                            <input type="text" class="form-control" name="celularE" id="celularE" placeholder="0000-0000" pattern="^[0-9-]+$" title="Debe de ingresar un número de celular válido">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="creditoE">Disponibilidad de crédito:</label>
                            <select name="creditoE" id="creditoE" class="form-control">
                                <option value="0">NO</option>
                                <option value="1">SI</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="limite_creditoE">Limite de crédito:</label>
                            <input type="text" class="form-control" disabled name="limite_creditoE" id="limite_creditoE" value="0.00">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-success" value="Guardar cambios">
                </div>
                <?php

                $actualizar = new ctrClientes;
                $actualizar->actualizarCliente();

                ?>
            </form>
        </div>

    </div>

</div>


<!-- Formulario de eliminar Cliente -->
<div class="modal fade" id="eliminarCliente">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Registro de Cliente</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <p>¿Esta seguro que desea eliminar el registro?</p>
                        <input type="hidden" id="codigoB" name="codigoB">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </div>
                <?php

                $eliminar = new ctrClientes;
                $eliminar->eliminarCliente();

                ?>
            </form>
        </div>
    </div>
</div>


<script>

    $(function() {
        $("#credito").change(function() {
            if ($(this).val() === "0") {
                $("#limite_credito").prop("disabled", true);
                $("#limite_credito").prop("value", "0.00");
            } else {
                $("#limite_credito").prop("disabled", false);
            }
        });

        $("#creditoE").change(function() {
            if ($(this).val() === "0") {
                $("#limite_creditoE").prop("disabled", true);
                $("#limite_creditoE").prop("value", "0.00");
            } else {
                $("#limite_creditoE").prop("disabled", false);
            }
        });
    });
</script>