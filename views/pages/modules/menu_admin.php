<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="" class="brand-link">
    <img src="/views/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">ALUVIR SYSTEM</span>
  </a>

  <div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo $_SESSION["IMAGEN_USUARIO"]; ?>" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="" class="d-block"><?php echo $_SESSION["NOMBRE_USUARIO"]; ?></a>
      </div>
    </div>
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href="home" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>Home</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="perfil" class="nav-link">
            <i class="nav-icon fas fa-user"></i>
            <p>Perfil</p>
          </a>
        </li>
        <?php
        // Módulo de personal
        $permiso = ctrAccesosSistema::comprobarAcceso("usuarios");
        if ($permiso === true) {
        ?>
          <li class="nav-item">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-users-cog"></i>
              <p>
                Personal
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="usuarios" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nuevo empleado</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="verUsuarios" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ver empleados</p>
                </a>
              </li>
            </ul>
          </li>
        <?php
        }
        ?>
        <?php
        // Módulo de clientes
        $permiso = ctrAccesosSistema::comprobarAcceso("clientes");
        if ($permiso === true) {
        ?>
          <li class="nav-item">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Clientes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="clientes" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Clientes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="direcciones" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Direcciones</p>
                </a>
              </li>
            </ul>
          </li>
        <?php
        }
        ?>
        <?php
        // Módulo de inventario
        $permiso = ctrAccesosSistema::comprobarAcceso("inventario");
        if ($permiso === true) {
        ?>
          <li class="nav-item">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Invetario
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="categorias" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Categorias</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="materiales" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Materiales</p>
                </a>
              </li>
            </ul>
          </li>
        <?php
        }
        ?>
        <?php
        // Módulo de compras
        $permiso = ctrAccesosSistema::comprobarAcceso("compras");
        if ($permiso === true) {
        ?>
          <li class="nav-item">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-shopping-basket"></i>
              <p>
                Compras
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="compras" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Registrar compra</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="registrarIngresoMaterial" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Registrar ingreso</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="proveedores" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Proveedores</p>
                </a>
              </li>
            </ul>
          </li>
        <?php
        }
        ?>
        <?php
        // Módulo de cotizaciones
        $permiso = ctrAccesosSistema::comprobarAcceso("cotizaciones");
        if ($permiso === true) {
        ?>
          <li class="nav-item">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Cotizaciones
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="realizarCotizacion" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Realizar cotización</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="cotizaciones" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Consultar cotización</p>
                </a>
              </li>
            </ul>
          </li>
        <?php
        }
        ?>
        <li class="nav-item">
          <a href="" class="nav-link">
            <i class="nav-icon fas fa-hand-paper"></i>
            <p>
              Orden de trabajo
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="ordenDeTrabajo" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Iniciar ODT</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="ordenesDeTrabajo" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Consultar ODT</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="" class="nav-link">
            <i class="nav-icon fas fa-cash-register"></i>
            <p>
              Ventas
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="registrarVenta" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Registrar Venta</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="ventas" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Consultar Venta</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="" class="nav-link">
            <i class="nav-icon fas fa-globe"></i>
            <p>
              Sistema
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="rolesSistema" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Roles</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="metodosPago" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Métodos de pago</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="" class="nav-link">
            <i class="nav-icon fas fa-handshake"></i>
            <p>
              Reportes
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="reportes" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Reportes</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#cerrarSesion">
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>Cerrar Sesión</p>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</aside>


<div class="modal fade" rol="dialog" id="cerrarSesion">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3>Cerrar Sesión</h3>
      </div>
      <div class="modal-body">
        <div class="box-body">
          <div class="form-group">
            <label for="">¿Deseas cerrar sesión definitivamente?</label>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="cerrarSession" class="btn btn-success"> Aceptar </a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cancelar</button>
      </div>
    </div>
  </div>
</div>