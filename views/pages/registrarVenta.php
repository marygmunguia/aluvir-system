<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>REGISTRAR VENTA</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">VENTA</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    Registrar venta de productos
                </div>
                <form method="post">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <label> Código de cotización:</label><br>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="form-group">
                                            <input type="text" value="<?php
                                                                        if (isset($_SESSION["VENTA"]["idCotizacion"])) {
                                                                            echo $_SESSION["VENTA"]["idCotizacion"];
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#seleccionarCotizacion">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label> Nombre del cliente:</label><br>
                                            <input type="text" value="<?php
                                                                        if (isset($_SESSION["VENTA"]["nombreCliente"])) {
                                                                            echo $_SESSION["VENTA"]["nombreCliente"];
                                                                        } else {
                                                                            echo "";
                                                                        }
                                                                        ?>" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Usuario de sistema:</label>
                                            <input type="text" value="<?php echo $_SESSION["NOMBRE_USUARIO"] ?>" disabled class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Fecha de cotización:</label>
                                            <input type="text" value="<?php echo date('d/m/Y'); ?>" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="row">
                                    <div class="col-12">
                                        <label>DETALLE DE PRODUCTOS</label>
                                        <table class="table table-striped registros">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10px">#</th>
                                                    <th>Nombre</th>
                                                    <th>Descripcion</th>
                                                    <th style="width: 20px">Cantidad</th>
                                                    <th style="width: 20px">Precio</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (isset($_SESSION["VENTA"]["detalleCotizacion"])) {
                                                    foreach ($_SESSION["VENTA"]["detalleCotizacion"] as $key => $value) {
                                                ?>
                                                        <tr>
                                                            <td><?php echo $value["COD_PRODUCTO"]; ?></td>
                                                            <td><?php echo $value["NOMBRE"]; ?></td>
                                                            <td><?php echo $value["DETALLE"]; ?></td>
                                                            <td><?php echo $value["CANT_PRODUCTO"]; ?></td>
                                                            <td><?php echo number_format($value["PRECIO_PRODUCTO"], 2); ?></td>
                                                            <td><?php echo number_format($value["TOTAL_PAGAR_PRODUCTO"], 2); ?></td>
                                                        </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <h5>DETALLES DE PAGO</h5>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Seleccionar el tipo de pago:</label>
                                                    <select name="tipoPago" id="tipoPago" class="form-control">
                                                        <option value="CONTADO">Contado</option>
                                                        <option value="CREDITO">Crédito</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label>Seleccionar el método de pago:</label>
                                                    <select name="metodoPago" id="metodoPago" class="form-control">
                                                        <?php
                                                        $respuesta = ctrMetodospago::consultarMetodospago();
                                                        foreach ($respuesta as $key => $value) {
                                                        ?>
                                                            <option value="<?php echo $value["PK_COD_METODO_PAGO"]; ?>"><?php echo $value["NOM_METODO_PAGO"]; ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Subtotal:</label>
                                            <input type="text" disabled class="form-control" value="<?php
                                                                                                    if (isset($_SESSION["VENTA"]["DETALLES"]["SUBTOTAL"])) {
                                                                                                        echo number_format($_SESSION["VENTA"]["DETALLES"]["SUBTOTAL"], 2);
                                                                                                    } else {
                                                                                                        echo '0.00';
                                                                                                    }
                                                                                                    ?>">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Descuento:</label>
                                            <input type="text" value="<?php
                                                                        if (isset($_SESSION["VENTA"]["DETALLES"]["DESCUENTO"])) {
                                                                            echo number_format($_SESSION["VENTA"]["DETALLES"]["DESCUENTO"], 2);
                                                                        } else {
                                                                            echo '0.00';
                                                                        }
                                                                        ?>" disabled class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Impuesto sobre venta:</label>
                                            <input type="text" value="<?php
                                                                        if (isset($_SESSION["VENTA"]["DETALLES"]["ISV"])) {
                                                                            echo number_format($_SESSION["VENTA"]["DETALLES"]["ISV"], 2);
                                                                        } else {
                                                                            echo '0.00';
                                                                        }
                                                                        ?>" disabled class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Total de cotización:</label>
                                            <input type="text" value="<?php
                                                                        if (isset($_SESSION["VENTA"]["DETALLES"]["TOTAL_PAGAR"])) {
                                                                            echo number_format($_SESSION["VENTA"]["DETALLES"]["TOTAL_PAGAR"], 2);
                                                                        } else {
                                                                            echo '0.00';
                                                                        }
                                                                        ?>" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-2">
                                    <button class="btn btn-block btn-danger">Cancelar Venta</button>
                                </div>
                                <div class="col-5"></div>
                                <div class="col-5">
                                    <button class="btn btn-block btn-success" type="submit">Realizar Venta</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $registrar = new ctrVentas;
                    $registrar->registrarVenta();
                    ?>
                </form>
            </div>
    </section>
</div>


<div class="modal fade" id="seleccionarCotizacion">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar Cotización</h4>
                </div>
                <div class="modal-body">
                    <table id="cotizaciones" style="width: 100%;" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>SubTotal</th>
                                <th>Descuento</th>
                                <th>ISV</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $respuesta = ctrVentas::consultarCotizacionesVenta();
                            foreach ($respuesta as $key => $value) {

                            ?>
                                <tr>
                                    <td><?php echo $value["PK_COD_COTIZACION"]; ?></td>
                                    <td><?php echo $value["NOM_CLIENTE"] . " " . $value["APE_CLIENTE"]; ?></td>
                                    <td><?php echo date_format(date_create($value["FEC_COTIZACION"]), "d-m-Y"); ?></td>
                                    <td><?php echo date_format(date_create($value["HORA_COTIZACION"]), "h:i a"); ?></td>
                                    <td><?php echo number_format($value["SUBTOTAL"], 2); ?></td>
                                    <td><?php echo number_format($value["DESCUENTO"], 2); ?></td>
                                    <td><?php echo number_format($value["ISV"], 2); ?></td>
                                    <td><?php echo number_format($value["TOTAL_PAGAR"], 2); ?></td>
                                    <td style="width: 10%;">
                                        <form method="post">
                                            <input type="hidden" name="idCotizacion" value="<?php echo $value["PK_COD_COTIZACION"]; ?>">
                                            <input type="hidden" name="codCliente" value="<?php echo $value["PK_COD_CLIENTE"]; ?>">
                                            <input type="hidden" name="nombreCliente" value="<?php echo $value["NOM_CLIENTE"] . " " . $value["APE_CLIENTE"]; ?>">
                                            <button class="btn btn-success" type="submit">
                                                <i class="fas fa-check"></i>
                                            </button>
                                            <?php
                                            $seleccionar = new ctrVentas;
                                            $seleccionar->seleccionarCotizacion();
                                            ?>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>
    $(document).ready(function() {
        $("#cotizaciones").DataTable({
            language: {
                sSearch: "Buscar:",
                sEmptyTable: "No hay datos disponibles",
                sZeroRecords: "No se han encontrado resultados",
                sInfo: "Mostrando registros del _START_ al _END_ de un total _TOTAL_",
                SInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0",
                sInfoFiltered: "(filtrando de un total de _MAX_ registrados)",
                oPaginate: {
                    sFirst: "Primero",
                    sLast: "Último",
                    sNext: "Siguiente",
                    sPrevious: "Anterior",
                },

                sLoadingRecords: "Cargando...",
                sLengthMenu: "Mostrar _MENU_ registros",
            },
            order: [
                [0, "desc"]
            ],
        });
    });


    $(function() {
        $("#tipoPago").change(function() {
            if ($(this).val() === "CREDITO") {
                $("#metodoPago").prop("disabled", true);
            } else {
                $("#metodoPago").prop("disabled", false);
            }
        })
    });
</script>