<?php
$permiso = ctrAccesosSistema::comprobarAcceso("usuarios");

if ($permiso === true) {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>GESTIÓN DE EMPLEADOS</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Empleados</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">

            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <table id="registros" class="table responsive table-bordered table-hover TB">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Usuario</th>
                                    <th>Correo Electrónico</th>
                                    <th>Dirección</th>
                                    <th>Telefono</th>
                                    <th>Celular</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $respuesta = ctrUsuarios::consultarEmpleados();
                                foreach ($respuesta as $key => $value) {

                                ?>
                                    <tr>
                                        <td><?php echo $value["PK_ID_USUARIO"];  ?></td>
                                        <td><?php echo $value["NOMBRE"]; ?></td>
                                        <td><?php echo $value["APELLIDO"]; ?></td>
                                        <td><?php echo $value["NOM_USUARIO"]; ?></td>
                                        <td><?php echo $value["EMAIL"]; ?></td>
                                        <td><?php echo $value["DIRECCION"]; ?></td>
                                        <td><?php echo $value["TELEFONO"]; ?></td>
                                        <td><?php echo $value["CELULAR"]; ?></td>
                                        <td><img class="img-circle" width="50px" src="<?php echo $value["IMG_USUARIO"]; ?>" alt="Imagen"></td>
                                        <?php

                                        if ($value["EST_USUARIO"] == "ACTIVO") {
                                        ?>

                                            <td style="width: 10%;">
                                                <center>
                                                    <button cod="<?php echo $value["PK_ID_USUARIO"]; ?>" type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#desactivarUsuario">
                                                        Desactivar
                                                    </button>
                                                </center>
                                            </td>

                                        <?php
                                        } else {
                                        ?>

                                            <td style="width: 10%;">
                                                <center>
                                                    <button cod="<?php echo $value["PK_ID_USUARIO"]; ?>" type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#desactivarUsuario">
                                                        Activar
                                                    </button>
                                                </center>
                                            </td>

                                        <?php
                                        }
                                        ?>
                                    </tr>
                                <?php
                                }
                                ?>
                        </table>
                    </div>

                </div>

            </div>



        </section>
    </div>
<?php
} else {
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>¡Error 403!</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Error 403</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-warning"> 403</h2>

                <div class="error-content">
                    <h3><i class="fas fa-exclamation-triangle text-warning"></i> Tu usuario no cuenta con los permiso. </h3>

                    <p>
                        No posee los permisos necesarios para ver el contenido. <a href="home">Regresar a inicio </a> del sistema.
                    </p>
                </div>
            </div>
        </section>
    </div>

    <script LANGUAGE="javascript">
        $(document).ready(function() {
            Swal.fire({
                title: 'Prohibido!',
                text: "Tu usuario NO cuenta con el permiso para acceder a este módulo",
                icon: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Volver'
            }).then((result) => {
                if (result.isConfirmed) {
                    <?php
                    $ruta = ctrRuta::cargarRuta();
                    ?>
                    window.location = "<?php echo $ruta; ?>home";
                }
            })
        });
    </script>
<?php
}
