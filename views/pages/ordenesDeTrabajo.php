<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>ORDENES DEL TRABAJO</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Orden de trabajo</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-body">
                <table id="registros" style="width: 100%;" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Fecha Entrega</th>
                            <th>Detalles</th>
                            <th>Estado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $respuesta = ctrVentas::consultarCotizacionesVenta();
                        foreach ($respuesta as $key => $value) {

                        ?>
                            <tr>
                                <td><?php echo $value["PK_COD_ORDEN_TRABAJO"]; ?></td>
                                <td><?php echo $value["NOM_CLIENTE"] . " " . $value["APE_CLIENTE"]; ?></td>
                                <td><?php echo date_format(date_create($value["FEC_ENTREGA"]), "d-m-Y"); ?></td>
                                <td><?php echo $value["DET_ORDEN_TRABAJO"]; ?></td>
                                <td><?php echo $value["ESTADO"]; ?></td>
                                <td>
                                    <center>
                                        <button type="button" codigo="<?php echo $value["PK_COD_COTIZACION"]; ?>" class="btn btn-primary btnImprimirCotizacion"><i class="fas fa-print"></i>
                                        </button>
                                    </center>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                </table>
            </div>
        </div>
    </section>
</div>