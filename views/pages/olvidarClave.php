<div class="login-box">
    <div class="login-logo">
    <a class="titulo-1" href="#"><b>ALUVIR</b> SYSTEM</a>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Ingresa tu correo electrónico para poder restablecer tu contraseña.</p>
            <form method="post">
                <div class="input-group mb-3">
                    <input type="email" class="form-control" placeholder="Correo Electrónico" name="email" require>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-1 btn-block">Restablecer contraseña</button>
                    </div>
                </div>
                <?php
                $usuario = new ctrUsuarios;
                $usuario -> RecuperarPassword();
                ?>
            </form>

            <p class="mt-3 mb-1">
                <a href="login" class="link-1">Iniciar sesión ahora!</a>
            </p>
        </div>
    </div>
</div>