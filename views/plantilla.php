<?php

$ruta = ctrRuta::cargarRuta();
date_default_timezone_set('America/Tegucigalpa');
session_start();

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ALUVIR SYSTEM</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $ruta; ?>views/plugins/fontawesome-free/css/all.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $ruta; ?>views/css/adminlte.min.css">
    <!-- DataTable -->
    <link rel="stylesheet" href="<?php echo $ruta; ?>views/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo $ruta; ?>views/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo $ruta; ?>views/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Estilos personalizados -->
    <link rel="stylesheet" href="<?php echo $ruta; ?>views/css/style.css">

    <!-- jQuery -->
    <script src="<?php echo $ruta; ?>views/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $ruta; ?>views/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SweetAlert 2 -->
    <script src="<?php echo $ruta; ?>views/plugins/sweetalert2/sweetalert2.all.min.js"></script>
    <!-- DataTable -->
    <script src="<?php echo $ruta; ?>views/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/jszip/jszip.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo $ruta; ?>views/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- Gráficos -->
    <script src="<?php echo $ruta; ?>views/plugins/chart.js/Chart.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

</head>

<body class="hold-transition sidebar-mini">
    <?php

    if (isset($_SESSION["INGRESAR"]) && $_SESSION["INGRESAR"] === 'ok') {

        if (isset($_GET["pagina"])) {
            $url = array();
            $url = explode("/", $_GET["pagina"]);

            if (
                $url[0] == "home" ||
                $url[0] == "perfil" ||
                $url[0] == "categorias" ||
                $url[0] == "clientes" ||
                $url[0] == "registrarCompra" ||
                $url[0] == "cotizaciones" ||
                $url[0] == "realizarCotizacion" ||
                $url[0] == "direcciones" ||
                $url[0] == "verUsuarios" ||
                $url[0] == "materiales" ||
                $url[0] == "ordenDeTrabajo" ||
                $url[0] == "ordenesDeTrabajo" ||
                $url[0] == "rolesSistema" ||
                $url[0] == "usuarios" ||
                $url[0] == "compras" ||
                $url[0] == "proveedores" ||
                $url[0] == "ventas" ||
                $url[0] == "registrarVenta" ||
                $url[0] == "registrarIngresoMaterial" ||
                $url[0] == "reportes" ||
                $url[0] == "metodosPago"
            ) {
                echo '<div class="wrapper">';
                include 'views/pages/modules/navbar.php';
                include 'views/pages/modules/menu_admin.php';
                include 'views/pages/' . $_GET["pagina"] . '.php';
                include 'views/pages/modules/footer.php';
                echo '</div>';
            } elseif ($url[0] == "cerrarSession") {
                include 'views/pages/' . $_GET["pagina"] . '.php';
            } else {
                echo '<div class="wrapper">';
                include 'views/pages/modules/navbar.php';
                include 'views/pages/modules/menu_admin.php';
                include 'views/pages/error404.php';
                include 'views/pages/modules/footer.php';
            }
        } else {

            echo '<div class="wrapper">';
            include 'views/pages/modules/navbar.php';
            include 'views/pages/modules/menu_admin.php';
            include 'views/pages/home.php';
            include 'views/pages/modules/footer.php';
            echo '</div>';
        }
    } else {

        if (isset($_GET["pagina"])) {
            $url = array();
            $url = explode("/", $_GET["pagina"]);
            if ($url[0] == "olvidarClave") {
                echo '<div class="login-page">';
                include 'views/pages/olvidarClave.php';
                echo '</div>';
            } else {
                echo '<div class="login-page">';
                include 'views/pages/login.php';
                echo '</div>';
            }
        } else {
            echo '<div class="login-page">';
            include 'views/pages/login.php';
            echo '</div>';
        }
    }
    ?>

    <!-- AdminLTE App -->
    <script src="<?php echo $ruta; ?>views/js/adminlte.min.js"></script>
    <!-- JS Personalizado  -->
    <script src="<?php echo $ruta; ?>views/js/main.js"></script>
    <script src="<?php echo $ruta; ?>views/js/datos/categorias/categorias.js"></script>
    <script src="<?php echo $ruta; ?>views/js/datos/clientes/clientes.js"></script>
    <script src="<?php echo $ruta; ?>views/js/datos/direcciones/direcciones.js"></script>
    <script src="<?php echo $ruta; ?>views/js/datos/cotizacion/cotizacion.js"></script>
    <script src="<?php echo $ruta; ?>views/js/datos/metodospago/metodospago.js"></script>
    <script src="<?php echo $ruta; ?>views/js/datos/Materiales/materiales.js"></script>
</body>

</html>