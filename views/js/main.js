// Activar y Traducir el dataTable
$("#registros").DataTable({
  language: {
    sSearch: "Buscar:",
    sEmptyTable: "No hay datos disponibles",
    sZeroRecords: "No se han encontrado resultados",
    sInfo: "Mostrando registros del _START_ al _END_ de un total _TOTAL_",
    SInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0",
    sInfoFiltered: "(filtrando de un total de _MAX_ registrados)",
    oPaginate: {
      sFirst: "Primero",
      sLast: "Último",
      sNext: "Siguiente",
      sPrevious: "Anterior",
    },

    sLoadingRecords: "Cargando...",
    sLengthMenu: "Mostrar _MENU_ registros",
  },
  responsive: true,
});

// PDF de cotizacion  //

$("#cotizaciones").on("click", ".btnImprimirCotizacion", function () {
  var codigo = $(this).attr("codigo");

  window.open(
    "tools/extension/tcpdf/examples/cotizaciones/cotizacion.php?codigo=" +
      codigo,
    "_black"
  );
});


$("#registros").on("click", ".btnImprimirCotizacion", function () {
  var codigo = $(this).attr("codigo");

  window.open(
    "tools/extension/tcpdf/examples/ordenDeTrabajo/ordenDeTrabajo.php?codigo=" +
      codigo,
    "_black"
  );
});

// Generar Reportes //

function reporteClientes() {
  window.open(
    "tools/extension/tcpdf/examples/reportes/reporteClientes.php",
    "_black"
  );
}

function reporteProveedores() {
  window.open(
    "tools/extension/tcpdf/examples/reportes/reporteProveedores.php",
    "_black"
  );
}

function reporteUsuarios() {
  window.open(
    "tools/extension/tcpdf/examples/reportes/reporteUsuarios.php",
    "_black"
  );
}

function reporteInventario() {
  window.open(
    "tools/extension/tcpdf/examples/reportes/reporteInventario.php",
    "_black"
  );
}
