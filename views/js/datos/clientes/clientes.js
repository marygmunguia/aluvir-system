// Editar Cliente

$("#registros").on("click", ".editarCliente", function () {
  $("#codigoE").val($(this).attr("cod"));
  $("#tipo_identificacionE").val($(this).attr("tipoIdentificacion"));
  $("#no_identificacionE").val($(this).attr("noIdentificacion"));
  $("#nombreE").val($(this).attr("nombre"));
  $("#apellidoE").val($(this).attr("apellido"));
  $("#correoE").val($(this).attr("email"));
  $("#telefonoE").val($(this).attr("telefono"));
  $("#celularE").val($(this).attr("celular"));
  $("#creditoE").val($(this).attr("credito"));
  $("#limite_creditoE").val($(this).attr("limiteCredito"));
});

// Eliminar Cliente

$("#registros").on("click", ".eliminarCliente", function () {
  $("#codigoB").val($(this).attr("cod"));
});
