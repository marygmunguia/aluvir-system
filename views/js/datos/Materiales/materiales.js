// Editar Materiales

$("#registros").on("click", ".editarMaterial", function () {
    $("#codigoE").val($(this).attr("cod"));
    $("#codBarraE").val($(this).attr("codBarra"));        
    $("#nombreE").val($(this).attr("nombre"));
    $("#detallesE").val($(this).attr("detalles"));
    $("#CodCategoriaE").val($(this).attr("codCategoria"));
    $("#precioVentaE").val($(this).attr("precioVenta"));    
    $("#minexisE").val($(this).attr("minexis"));
    $("#maxexisE").val($(this).attr("maxexis"));
  });
  
  // Eliminar Materiales
  
  $("#registros").on("click", ".eliminarMaterial", function () {
    $("#codigoB").val($(this).attr("cod"));
  });
  